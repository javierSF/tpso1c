/*
 * libMemory.c
 *
 * Created on: 24/04/2013
 * Author: mcrosignani
 * */

#include "koopa.h"
#include <stdlib.h>
#include <string.h>
#include <commons/log.h>

typedef struct t_listParts{
	particion *part;
	struct t_listParts *next;
}listParts;

//Busca un espacio libre para tamanio dentro de segmento
t_memoria srchFreeSeg(t_memoria segmento, int tamanio);

//Arma un struct particion con id tamanio y dato
//particion* buildPartition(char id, int tamanio, char* dato);
particion* buildPartition(char id, int tamanio);

//Busca la particion dentro de una lista
int findPartition(particion* part, listParts* particiones_almacenadas);

//Guarda la particion en la lista
void savePartition(particion* part, t_memoria segmento);

//Retorna la siguiente particion almacenada de la lista de particiones,
//si no hay ninguna particion (lista vacia) retorna una particion cuyo dato es un puntero == _pult
particion getNextPart(char id);

//Libera la memoria tomada
void destruirLista(listParts *lista);

#define TRUE 1
#define FALSE 0
#define LOG_PATH "./logKoopa.txt"

static int TAMANIO_SEG;
static listParts *_storedParts;
static t_memoria _plast; //Puntero al final de la ultima particion almacenada
static char _idLastPart;

t_memoria crear_memoria(int tamanio){
	//Reservo el espacio para el segmento
	char *p = (char *)malloc(sizeof(char) * tamanio);

	//Si no es null, encontro espacio
	if(p != NULL){
		//Me guardo el tamanio y creo la lista para las particiones
		TAMANIO_SEG = tamanio;
		_plast = p;
	}
	else {
		//Evaluar que hacer en este caso (ERROR)
	}

	return p;
}

int almacenar_particion(t_memoria segmento, char id, int tamanio, t_memoria contenido){
	t_memoria freeSeg = NULL;

	t_log *logFile = log_create(LOG_PATH, "ProcesoKoopa", false, LOG_LEVEL_INFO);

	//Si el tamanio es mayor al tamanio total de la particion
	if(tamanio > TAMANIO_SEG){
		return -1;
	}

	//Construyo la particion
	particion *part = buildPartition(id, tamanio);

	//Si no grabe una particion con igual id en la lista
	//busco lugar en el segmento para guardar el dato
	if(findPartition(part, _storedParts) == FALSE){
		freeSeg = srchFreeSeg(segmento, tamanio);
	}
	else{
		return -1;
	}

	//Si hay espacio, guardo el dato y guardo la particion en la lista
	if(freeSeg != NULL){
		memcpy(freeSeg, contenido, tamanio);
		part -> dato = freeSeg;
		part -> libre = FALSE;
		part -> inicio = part -> dato - segmento;
		savePartition(part, segmento);
		_plast = freeSeg + tamanio;
		_idLastPart = id;
	}
	else {
		//No encontro espacio libre en el segmento
		log_destroy(logFile);
		return 0;
	}
	log_destroy(logFile);
	return 1;
}

particion* buildPartition(char id, int tamanio){
	particion *part = malloc(sizeof(particion));

	part -> id = id;
	part -> tamanio = tamanio;
	part -> libre = TRUE;
	part -> inicio = 0;

	return part;
}

int findPartition(particion *part, listParts *particiones_almacenadas){
	while(particiones_almacenadas != NULL){
		if(particiones_almacenadas -> part -> id == part -> id){
			return 1;
		}

		particiones_almacenadas = particiones_almacenadas -> next;
	}

	return 0;
}

t_memoria srchFreeSeg(t_memoria segmento, int tamanio) {
	t_memoria paux = _plast;
	t_memoria pendSeg = segmento + TAMANIO_SEG;
	particion next;
	int space;
	char id = _idLastPart;

	while(paux < pendSeg){
			next = getNextPart(id);

			if((next.id) != id){//Se puede cambiar comparando inicios en vez de punteros
				space = (next.dato) - paux;

				if(space >= tamanio){
					return paux;
				}

				paux = (next.dato) + (next.tamanio);
				id = next.id;
			}
			else {//next.id == id
				//Calculo el espacio con el final del segmento
				//si es suficiente retorno, sino empiezo a buscar al principio del segmento
				space = pendSeg - paux;

				if(space >= tamanio){
					return paux;
				}

				break;
			}
		}

	//Empiezo a buscar desde el principio
	//Seteo id nulo
	id = '\0';//Para que la funcion me retorne la primer particion
	next = getNextPart(id);
	paux = segmento;

	if(next.tamanio != -1){//Hay algo en la lista
		if(paux == next.dato){
			if(next.libre == TRUE){
				space = next.tamanio;

				if(space >= tamanio){
					return paux;
				}
			}
			paux = paux + (next.tamanio);
		}
		else {
			space = (next.dato) - paux;

			if(space >= tamanio){
				return paux;
			}

			paux = (next.dato) + (next.tamanio);
			id = next.id;
		}
	}
	else{
		//La lista vuelve a estar vacia
		space = TAMANIO_SEG;

		if(space >= tamanio){
			return paux;
		}

		return NULL;
	}

	while(paux < _plast){
		next = getNextPart(id);

		if(next.libre == TRUE){
			space = next.tamanio;

			if(space >= tamanio){
				return paux;
			}
		}

		paux = (next.dato) + (next.tamanio);
		id = next.id;
	}

	return NULL;
}

particion getNextPart(char id){
	particion part;
	int encontrado = 0;
	listParts *list = _storedParts;

	//Retorno la primera particion de la lista (si hay)
	if(id == '\0'){
		if(list != NULL){

				part.dato = list -> part -> dato;
				part.id = list -> part -> id;
				part.tamanio = list -> part -> tamanio;
				part.libre = list->part->libre;

				return part;
		}

		//Particion nula
		part.tamanio = -1;

		return part;
	}

	part.dato = _plast;
	part.id = _idLastPart;
	part.libre = FALSE;

	while(list != NULL && encontrado == 0){
		if (list -> part -> id != id){
			list = list -> next;
		}
		else {
			encontrado = 1;
		}
	}

	if(list != NULL){
		list = list -> next;

		if (list != NULL){
			part.dato = list -> part -> dato;
			part.id = list -> part -> id;
			part.tamanio = list -> part -> tamanio;
			part.libre = list->part->libre;
		}
	}

	return part;
}

void savePartition(particion* part, t_memoria segmento){
	 	listParts *nuevoNodo;
	 	listParts *pant = _storedParts;
	    listParts *auxiliar = _storedParts;
	    nuevoNodo =  malloc(sizeof(listParts));

	    nuevoNodo->part = part;
	    nuevoNodo->next = NULL;

	    if (_storedParts == NULL) {
	        _storedParts = nuevoNodo;
	        return;
	    }
	    else {
	        while(auxiliar != NULL && (auxiliar -> part -> inicio < part -> inicio)) {
	            pant = auxiliar;
	        	auxiliar =  auxiliar->next;
	        }

	        if (auxiliar == NULL){
	        	auxiliar = nuevoNodo;
	        	pant -> next = auxiliar;
	        }
	        else {
	        	if(auxiliar->part->inicio == part->inicio){
	        		//Sustituir particion
	        		//almacenar
	        		nuevoNodo->next = auxiliar;
	        		pant->next = nuevoNodo;

	        		//Actualizar
	        		auxiliar->part->inicio = nuevoNodo->part->inicio + nuevoNodo->part->tamanio;
	        		auxiliar->part->tamanio = auxiliar->part->tamanio - nuevoNodo->part->tamanio;
	        		auxiliar->part->dato = segmento + auxiliar->part->inicio;
	        		return;
	        	}

	        	nuevoNodo->next = auxiliar;
	        	pant->next = nuevoNodo;
	        }

	        return;
	    }
}

int eliminar_particion(t_memoria segmento, char id){
	listParts *paux = _storedParts;

	while (paux != NULL){
		if (paux -> part -> id == id && paux -> part -> libre == FALSE){
			paux -> part -> libre = TRUE;
			break;
		}
		paux = paux -> next;
	}

	if (paux == NULL){
		//Recorrio toda la lista sin encontrar una particion ocupada con ese id
		return 0;
	}

	return 1;
}

void liberar_memoria(t_memoria segmento){
	if (segmento != NULL){
		free(segmento);
		destruirLista(_storedParts);
	}
}

void destruirLista(listParts *lista){
	listParts *elemento;

	while(lista != NULL){
		elemento = lista;
		free(elemento);
		lista = lista->next;
	}

	free(lista);
}

t_list* particiones(t_memoria segmento){
	listParts *paux = _storedParts;
	t_list *list = list_create();
	particion *part;
	int inicio = 0;

	while (paux != NULL){

			if(paux->part->inicio == inicio){
				//inicio = inicio + paux->part->tamanio;
			}
			else{
				part = malloc(sizeof(particion));

				part->id = '\0';
				part->inicio = inicio;
				part->tamanio = paux->part->inicio - inicio;
				part->libre = TRUE;

				list_add(list, part);

				inicio = inicio + part->tamanio;
			}

			part = malloc(sizeof(particion));

			part->id = paux -> part->id;
			part->inicio = paux -> part->inicio;
			part->tamanio = paux -> part->tamanio;
			part->libre = paux -> part->libre;
			part->dato = paux -> part->dato;

			list_add(list, part);
			inicio = inicio + paux->part->tamanio;

			paux = paux->next;
	}

	if(paux == NULL && inicio < TAMANIO_SEG){
		part = malloc(sizeof(particion));

		part->id = '\0';
		part->dato = segmento + inicio;
		printf("%d\n", part->dato - segmento);
		part->inicio = inicio;
		part->tamanio = TAMANIO_SEG - inicio;
		part->libre = TRUE;

		list_add(list, part);
	}

	return list;
}
