/*
 * koopa.h
 *
 *  Created on: 25/04/2013
 *      Author: mcrosignani
 */

#ifndef KOOPA_H_
#define KOOPA_H_

#include <commons/collections/list.h>

typedef char* t_memoria;

typedef struct t_particion {
	char id;
	int inicio;
	int tamanio;
	t_memoria dato;
	bool libre;
} particion; //Estructura que define una particion

//Crea el segmento de memoria a particionar
t_memoria crear_memoria(int tamanio);

//Crea una particion dentro del segmento de memoria
int almacenar_particion(t_memoria segmento, char id, int tamanio,
		t_memoria contenido);

//Elimina la particion dentro del segmento de memoria
int eliminar_particion(t_memoria segmento, char id);

//Libera los recursos tomados
void liberar_memoria(t_memoria segmento);

//Devuelve una lista
t_list* particiones(t_memoria segmento);

#endif /* KOOPA_H_ */
