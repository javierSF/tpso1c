#include "Planificador.h"
#include "commons/string.h"
#define BACKLOG 5

int _nivelSockfd;

void planificador(datos_planificador_t *datos) {
	logFile = log_create(LOG_PATH, "HiloPlanificador", false,
			log_level_from_string(configObj->detalleLog));
	int listener = datos->socket; //Socket de escucha para nuevas conexiones
	int notifyfd = getNotifyFileDescriptor();
	fd_set readEscuchafds, masterEscuchafds; //Conjunto de lectura
	int fdmax, sockfd, newfd = 0; //Maximo descriptor
	personaje_bloqueado_t* personajeEnMovimiento = NULL;
	FD_ZERO(&readEscuchafds);
	FD_ZERO(&masterEscuchafds);
	FD_SET(listener, &masterEscuchafds); //Cargo SETs
	FD_SET(notifyfd, &masterEscuchafds);
	fdmax = listener;

	if (notifyfd > fdmax) {
		fdmax = notifyfd;
	}

	while (1) {
		printf("escuchando en ip: %s port: %s\n", datos->ipInfo->addr,
				datos->ipInfo->port);
		readEscuchafds = masterEscuchafds;

		switch (selectTimeOut(configObj->wait, fdmax, &readEscuchafds)) {
		case 0:	//Poner a mover un personaje
			personajeEnMovimiento = obtenerProximoPersonaje(datos);
			if (personajeEnMovimiento == NULL ) {
				break;
			}
			if (FD_ISSET(personajeEnMovimiento->sockfd, &masterEscuchafds)) {
				int nbytes = moverPersonaje(personajeEnMovimiento, datos);
				if (nbytes == -1) {
					log_error(logFile,
							"%c se desconectó imprevistamente de %s.\n",
							personajeEnMovimiento->simbolo, datos->nombre);
					close(personajeEnMovimiento->sockfd);
					FD_CLR(personajeEnMovimiento->sockfd, &masterEscuchafds);
					rutinaPersonajeDesconectado(personajeEnMovimiento, datos);
				}
			}
			break;
		case 1:	//Atender pedido según socket.
			for (sockfd = 0; sockfd <= fdmax; sockfd++) {
				if (FD_ISSET(sockfd, &readEscuchafds)) {
					if (sockfd == listener) {
						newfd = aceptarConexion(listener, datos);
						if (newfd != 0) {
							FD_SET(newfd, &masterEscuchafds);
							if (newfd > fdmax)
								fdmax = newfd;
						}
					} else if (sockfd == notifyfd) {
						tratarModificacion(notifyfd);
					} else {
						tratarPedido(sockfd, datos);
					}
				}
			}
			break;
		case -1:	//Error
			log_error(logFile, "Funcion select del planificador: %s",
					datos->nombre);
			break;
		default:
			break;
		}

	}
	close(notifyfd);
}

void tratarPedido(int sockfd, datos_planificador_t *datos) {
	header_t header;

	recv(sockfd, &header, sizeof(header), MSG_WAITALL);

	switch (header.type) {
	case NOTIFICACION_REINICIAR_NIVEL:
		tratarReinicioNivel(sockfd, datos);
		break;
	}
}

void tratarReinicioNivel(int sockfd, datos_planificador_t *datos) {
	int _is_personaje(personaje_bloqueado_t *personaje) {
		return personaje->sockfd == sockfd;
	}
	//TODO:implementar mutex de colas compartidas.
	personaje_bloqueado_t *personaje = list_remove_by_condition(
			datos->personajesBloqueados->elements, (void *) _is_personaje);

	if (personaje != NULL ) {
		queue_push(datos->personajesListos, personaje);
	}
}

int aceptarConexion(int listener, datos_planificador_t *datos) {
	header_t header;
	int newsock = 0, nbytes;

	newsock = sockets_accept(listener);

	if (newsock == -1) {
		log_error(logFile, "No se puedo aceptar el listener");
		return EXIT_FAILURE;
	}

	nbytes = recv(newsock, &header, sizeof(header), MSG_WAITALL);

	if (header.type == HANDSHAKE_PERSONAJE) {
		nbytes = aceptarNuevoPersonaje(newsock, datos);
	}

	if (nbytes == 0)
		newsock = 0;

	return newsock;
}

int aceptarNuevoPersonaje(int persSockfd, datos_planificador_t *datos) {
	personaje_bloqueado_t* personaje;
	int nbytes = enviarHandshakePlanificador(persSockfd);
	personaje = recibirInfoPersonaje(persSockfd);
	log_info(logFile, "Pesonaje: %c conectado", personaje->simbolo);
	if (nbytes != 0 || personaje != 0) {
		queue_push(datos->personajesListos, personaje);
	}

	return nbytes;
}

void aceptarNivel(int nivSockfd) {
	enviarHandshakePlanificador(nivSockfd);	//Envio el handshake para confirmar la conexion

	_nivelSockfd = nivSockfd;
}

personaje_bloqueado_t* crearPersonaje(char idP, char idR, int sockfd) {
	personaje_bloqueado_t* temp;
	temp = malloc(sizeof(personaje_bloqueado_t));
	temp->simbolo = idP;
	temp->recursoPedido = idR;
	temp->sockfd = sockfd;
	return temp;
}

int enviarHandshakePlanificador(int sockfd) {
	header_t header;
	header.type = HANDSHAKE_PLANIFICADOR;
	header.length = 0;

	return sockets_send(sockfd, &header, '\0');
}

personaje_bloqueado_t* recibirInfoPersonaje(int newsock) {
	header_t header;
	personaje_bloqueado_t* personaje;
	char* data;
	int nbytes;

	nbytes = recv(newsock, &header, sizeof(header), MSG_WAITALL);

	if (header.type == NOTIFICACION_DATOS_PERSONAJE) {
		data = malloc(header.length);
		nbytes = recv(newsock, data, header.length, MSG_WAITALL);

	}

	if (nbytes == 0) {
		personaje = 0;
	} else {
		personaje = crearPersonaje(data[0], ' ', newsock);
	}

	free(data);

	return personaje;
}

//Retorna 0 si el mov fue correcto, 1 si no se pudo mover, -1 en caso de error
int evaluarRespuestaMovPersonaje(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos) {
	header_t header;
	int respuesta, nbytes;
	nbytes = recv(personajeEnMovimiento->sockfd, &header, sizeof(header),
			MSG_WAITALL);

	if (nbytes > 0) {
		switch (header.type) {
		case NOTIFICACION_MOVIMIENTO_REALIZADO:
			respuesta = tratarNotificacionMovimento(personajeEnMovimiento,
					datos, header);
			break;
		case NOTIFICACION_BLOQUEO:
			respuesta = tratarNotificacionBloqueo(personajeEnMovimiento, datos,
					header);
			break;
		case NOTIFICACION_NIVEL_FINALIZADO:
			//TODO:FD_CLR(personajeEnMovimiento->sockfd, &personajefds);
			respuesta = finalizarPersonaje(personajeEnMovimiento, datos,
					header);

			//Si terminaron todos los personajes ejecuto Koopa
			if (datos->personajesListos->elements->elements_count == 0
					&& datos->personajesBloqueados->elements->elements_count
							== 0) {
				ejecutarKoopa(configObj->solicitudesKoopa);
			}
			break;
		default:
			log_error(logFile,
					"Mensaje inesperado para planificador: %s por parte del nivel",
					datos->nombre);
			break;
		}
	} else {
		respuesta = -1;
	}

	return respuesta;
}

//Le envio el mensaje para que se mueva, seteo la variable persEnMov y lo agrego al set de readfds
void enviarMensajeDeMovimiento(personaje_bloqueado_t* personajeEnMovimiento) {
	header_t header;
	header.type = NOTIFICACION_MOVIDA;
	header.length = 0;

	if ((sockets_send(personajeEnMovimiento->sockfd, &header, '\0')) == -1) {
		log_error(logFile, "Se perdio conexion con el personaje");
	}
}

personaje_bloqueado_t* obtenerProximoPersonaje(datos_planificador_t* datos) {
	personaje_bloqueado_t *pers = NULL;
	if (!queue_is_empty(datos->personajesListos)) {
		pers = queue_pop((datos->personajesListos));//Saco al personaje de la cola. Cuando termina el turno tengo que volver a insertarlo
	}
	return pers;
}

/**
 *  retorna 0 timeout, 1 avaible, -1 error.
 */
int selectTimeOut(unsigned int segundos, int fdmax, fd_set* escuchafds) {
	struct timeval timeout;

	timeout.tv_sec = segundos;
	timeout.tv_usec = 0;

	return (select(fdmax + 1, escuchafds, NULL, NULL, &timeout));
}

int moverPersonaje(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos) {
	int quamtum = configObj->quantum;
	int respuesta;

	while (quamtum > 0) {
		enviarMensajeDeMovimiento(personajeEnMovimiento);
		respuesta = evaluarRespuestaMovPersonaje(personajeEnMovimiento, datos);
		if (respuesta) {
			break;
		}
		quamtum--;
	}
	if (quamtum == 0) {
		queue_push(datos->personajesListos, personajeEnMovimiento);
	}

	return respuesta;
}

int tratarNotificacionBloqueo(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, header_t header) {
	int respuesta = 1;
	char *data = malloc(header.length);
	recv(personajeEnMovimiento->sockfd, data, header.length, MSG_WAITALL);
	personajeEnMovimiento->recursoPedido = data[0];
	queue_push(datos->personajesBloqueados, personajeEnMovimiento);
	logDeListas(personajeEnMovimiento, datos->personajesListos,
			datos->personajesBloqueados);
	// TODO:free(personajeEnMovimiento);???
	free(data);
	return respuesta;
}

int finalizarPersonaje(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, header_t header) {
	int respuesta = 1;
	sacarPersonajeCola(personajeEnMovimiento, datos->personajesBloqueados);
	sacarPersonajeCola(personajeEnMovimiento, datos->personajesListos);
	logDeListas(personajeEnMovimiento, datos->personajesListos,
			datos->personajesBloqueados);
	free(personajeEnMovimiento);
	return respuesta;
}

int tratarNotificacionMovimento(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, header_t header) {
	int respuesta = 0;

	return respuesta;
}

void sacarPersonajeCola(personaje_bloqueado_t* personaje, t_queue* cola) {

	t_list* elementos = cola->elements;
	int _is_personaje(personaje_bloqueado_t *p) {
		return (p->simbolo) == (personaje->simbolo);
	}

	recurso_t *recursoLiberado = NULL;
	recursoLiberado = list_remove_by_condition(elementos,
			(void*) _is_personaje(personaje));
	if (recursoLiberado != NULL ) {
		free(recursoLiberado);
	}

}

void logDeListas(personaje_bloqueado_t* personajeEnMovimiento, t_queue* listos,
		t_queue* bloqueados) {

	int i = 0;
	char* listaString = malloc(sizeof("Listos: ") + 1);
	strcpy(listaString, "Listos: ");
	personaje_bloqueado_t* personajeEnCola;
	for (i = 0; i < listos->elements->elements_count; i++) {
		personajeEnCola = list_get(listos->elements, i);
		string_append_with_format(&listaString, "%<-",
				personajeEnCola->simbolo);
	}

	string_append(&listaString, "Bloqueados: ");

	for (i = 0; i < bloqueados->elements->elements_count; i++) {
		personajeEnCola = list_get(bloqueados->elements, i);
		string_append_with_format(&listaString, "%<-",
				personajeEnCola->simbolo);
	}

	string_append(&listaString, "Ejecutando: ");
	string_append(&listaString, &personajeEnMovimiento->simbolo);

	log_info(logFile, listaString);
	free(listaString);
}

int getNotifyFileDescriptor() {
	int file_descriptor = inotify_init();
	if (file_descriptor < 0) {
		log_error(logFile, "Al inicializar el inotify");
	} else {
		log_info(logFile, "Inotify inicializado");
	}

	int watch_descriptor = inotify_add_watch(file_descriptor, CONF_PATH,
			IN_MODIFY);
	if (watch_descriptor < 0) {
		log_error(logFile, "Al agregar el watch");
	} else {
		log_info(logFile, "Watch agregado");
	}

	return file_descriptor;

}

void tratarModificacion(int file_descriptor) {

	char buffer[BUF_LEN];
	int length = read(file_descriptor, buffer, BUF_LEN);
	if (length < 0) {
		log_error(logFile, "Notify read");
	}
	int offset = 0;
	while (offset < length) {

		struct inotify_event *event = (struct inotify_event *) &buffer[offset];

		if (event->mask & IN_MODIFY) {
			if (!(event->mask & IN_ISDIR)) {
				actualizarQuantum();
				log_info(logFile, "cambio de quantum: %d\n",
						configObj->quantum);
			}
		}
		offset += sizeof(struct inotify_event) + event->len;
	}
}

void actualizarQuantum(void) {
	configObj = malloc(sizeof(PLATAFORMA_CONF));
	t_config *conf = config_create(CONF_PATH);

	if (config_has_property(conf, "QUANTUM")) {
		configObj->quantum = config_get_int_value(conf, "QUANTUM");
	}
	config_destroy(conf);
}

void ejecutarKoopa(char *solicitudes) {
	char c;
	char *comando;

	printf("Desea enfrentar a Koopa? y/n");

	if ((c = getchar()) == 'y') {
		comando = strcpy("./koopa ", solicitudes);
		system(comando);
	}
}
