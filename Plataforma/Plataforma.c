/*
 * Plataforma.c
 *
 *  Created on: 23/04/2013
 *      Author: utnso
 */
#include "Plataforma.h"
#include "Planificador.h"

int main(void) {
	getConfiguracion();
	pthread_t horquestador;
	pthread_create(&horquestador, NULL, (void*) orquestador, NULL );
	pthread_join(horquestador, (void **) NULL );
	configObj_destroy(configObj);
	return EXIT_SUCCESS;
}

int atenderPedido(int sockfd) {
	header_t header;
	char *data;
	int nbytes;

	nbytes = recv(sockfd, &header, sizeof(header), MSG_WAITALL);

	if (nbytes == -1)
		log_error(logFile, "Conexion pedido.");

	switch (header.type) {
	//Pedidos Nivel
	case NOTIFICACION_RECURSOS_LIBERADOS:
		//Leer recursos liberados del Nivel
		//Desbloquear procesos que esperan esos recursos
		//Actualizar contador de recursos disponibles al nivel
		if (header.length == 0) {		//No se puede hacer recv de 0 bytes.
			notificarRecursosUsados(sockfd, NULL );
		} else {
			data = malloc(header.length);
			nbytes = recv(sockfd, data, header.length, MSG_WAITALL);

			if (nbytes != 0) {
				t_list *recursosLiberados = listaRecursos_deserializer(data,
						header.length);
				atenderRecursosLiberados(sockfd, recursosLiberados);
			}
			free(data);
		}
		break;
	case NOTIFICACION_PERSONAJES_ITERBLOQUEADOS:
		data = malloc(header.length);
		nbytes = recv(sockfd, data, header.length, MSG_WAITALL);
		if (nbytes != 0) {
			t_list *personajesInterbloqueados =
					personajesInterbloqueados_deserializer(data, header.length);
			hacerRecovery(personajesInterbloqueados, sockfd);
		}
		free(data);
		break;
		//Pedidos Personaje
	case NOTIFICACION_NIVEL_FINALIZADO:
		break;
	default:
		matarHilo(sockfd);
		log_error(logFile, "Pedido incorrecto socket: %d", sockfd);
		break;
	}

	return nbytes;
}

void matarHilo(int sockfd) {
	char *key = getPlanificadorKey(sockfd);
	datos_planificador_t *unPlanificador = dictionary_remove(dic_planificadores,
			key);
	nivel_info_t *conexion = dictionary_remove(dic_conexiones,
			unPlanificador->nombre + 3);
	ipInfo_destroy(conexion->addrNivel);
	free(conexion->addrPlanificador);
	free(conexion);
	pthread_cancel(*unPlanificador->hPlanificador);
	queue_destroy_and_destroy_elements(unPlanificador->personajesBloqueados,
			free);
	queue_destroy_and_destroy_elements(unPlanificador->personajesListos, free);
	free(unPlanificador->masterfds);
	free(unPlanificador->ipInfo);
	free(unPlanificador->nombre);
	free(unPlanificador->hPlanificador);
	free(unPlanificador);
	free(key);
}

void atenderRecursosLiberados(int nivelSockfd, t_list *recursosLiberados) {
	t_list *recursosUsados = desbloquearPersonajes(nivelSockfd,
			recursosLiberados);
	listaRecursos_destroy(recursosLiberados);
	notificarRecursosUsados(nivelSockfd, recursosUsados);
	listaRecursos_destroy(recursosUsados);
}

void hacerRecovery(t_list *personajesInterbloqueados, int nivelSockfd) {

	datos_planificador_t* infoPlanificador = findPlanificador(nivelSockfd);
	personaje_bloqueado_t* primerPersonaje = buscarPrimerPersonajeConectado(
			personajesInterbloqueados, infoPlanificador);
	enviarNotificacionMuerte(primerPersonaje);
	printf(
			"Se eligio matar al personaje %c para atender el interbloqueo del nivel %s\n",
			primerPersonaje->simbolo, infoPlanificador->nombre);
	pthread_mutex_lock(infoPlanificador->mutexColasCompartidas);
	rutinaPersonajeDesconectado(primerPersonaje->sockfd, infoPlanificador);
	pthread_mutex_unlock(infoPlanificador->mutexColasCompartidas);
}

personaje_bloqueado_t* buscarPrimerPersonajeConectado(t_list* personajesDL,
		datos_planificador_t* planificador) {
	int i, flag = 1;

	t_list* bloqueados = planificador->personajesBloqueados->elements;
	personaje_bloqueado_t* menor;

	//pthread_mutex_lock(planificador->mutexColasCompartidas);//TODO: mutex lock.

	for (i = 0; i < personajesDL->elements_count; i++) {//Recorro los personajes que se encuentran en DL
		personaje_interbloqueado_t* personajeDL = list_get(personajesDL, i);
		int _es_personaje(personaje_interbloqueado_t *personaje) {
			return personaje->id == personajeDL->id;
		}
		personaje_bloqueado_t* personajeB = list_find(bloqueados,
				(void *) _es_personaje);// Lo busco en bloqueados para obtener el soskfd

		if (flag) {		//Si es el primero asumo que es el menor
			menor = personajeB;
			flag = 0;
		} else { // Si no lo comparo con el menor
			if (menor->sockfd > personajeB->sockfd) { // si es menor pasa a ser el nuevo menor
				menor = personajeB;
			}
		}
	}
	//pthread_mutex_lock(planificador->mutexColasCompartidas);//TODO: mutex unlock.

	return menor;
}

void enviarNotificacionMuerte(personaje_bloqueado_t* unPersonaje) {
	header_t header;
	int bytes;
	header.type = NOTIFICACION_MUERTE;
	header.length = 0;

	bytes = sockets_send(unPersonaje->sockfd, &header, '\0');
	if (bytes == 0) {
		log_error(logFile, "Personaje desconectado");
	}

}

void notificarRecursosUsados(int sockfd, t_list *recursosUsados) {
	header_t header;
	int bytes;
	header.type = NOTIFICACION_RECURSOS_ASIGNADOS;

	if (recursosUsados == NULL ) {
		header.length = 0;
		bytes = sockets_send(sockfd, &header, '\0');
	} else {
		char *data = listaPersonajeDesbloqueado_serializer(recursosUsados,
				&header.length);
		bytes = sockets_send(sockfd, &header, data);
		free(data);
	}

	if (bytes == 0) {
		log_error(logFile, "Nivel desconectado");
	}
}

datos_planificador_t *findPlanificador(int nivelSockfd) {
	char *sockString = malloc(sizeof(int));

	sprintf(sockString, "%d", nivelSockfd);
	datos_planificador_t *unPlanificador = dictionary_get(dic_planificadores,
			sockString);
	free(sockString);

	return unPlanificador;
}

t_list *desbloquearPersonajes(int nivelSockfd, t_list *recursosLiberados) {
	t_list *recursosUsados = list_create();
	datos_planificador_t *unPlanificador = findPlanificador(nivelSockfd);

	if (pthread_mutex_lock(unPlanificador->mutexColasCompartidas))
		log_error(logFile, "mutex_lock desbloquearPersonajes");
	if (!queue_is_empty(unPlanificador->personajesBloqueados)) {
		int i, j;
		t_list *personajesDesbloqueados = list_create();
		personaje_bloqueado_t *unPersonajeBloqueado;
		personaje_bloqueado_t *unPersonajeDesbloqueado;
		t_list *personajesBloqueados =
				unPlanificador->personajesBloqueados->elements;

		for (i = 0; i < personajesBloqueados->elements_count; i++) {
			unPersonajeBloqueado = list_get(personajesBloqueados, i);
			int fueUsado = usarRecurso(unPersonajeBloqueado->recursoPedido,
					recursosLiberados, recursosUsados, unPersonajeBloqueado);

			if (fueUsado) {
				list_add(personajesDesbloqueados, unPersonajeBloqueado);
				informarDesbloqueo(unPersonajeBloqueado, unPlanificador);
				if (recursosLiberados->elements_count == 0)
					break;
			}
		}

		for (j = 0; j < personajesDesbloqueados->elements_count; j++) {
			unPersonajeDesbloqueado = list_get(personajesDesbloqueados, j);
			desbloquearPersonaje(unPersonajeDesbloqueado, unPlanificador);
		}

		list_destroy(personajesDesbloqueados);
	}
	if (pthread_mutex_unlock(unPlanificador->mutexColasCompartidas))
		log_error(logFile, "mutex_unlock Desbloquear personajes");

	return recursosUsados;
}

void desbloquearPersonaje(personaje_bloqueado_t *unPersonaje,
		datos_planificador_t *unPlanificador) {
	int _is_personaje(personaje_bloqueado_t *personaje) {
		return personaje->simbolo == unPersonaje->simbolo;
	}

	t_list *personajesBloqueados =
			unPlanificador->personajesBloqueados->elements;
	t_queue *personajesListos = unPlanificador->personajesListos;
	personaje_bloqueado_t *personajeDesbloqueado = list_remove_by_condition(
			personajesBloqueados, (void *) _is_personaje);
	personajeDesbloqueado->recursoPedido = ' ';
	queue_push(personajesListos, personajeDesbloqueado);
	logDeListas(personajeDesbloqueado, unPlanificador->personajesListos,
				unPlanificador->personajesBloqueados);
}

int usarRecurso(char simboloRecurso, t_list *recursosLiberados,
		t_list *recursosUsados, personaje_bloqueado_t *unPersonaje) {
	int _is_recurso(recurso_t *recurso) {
		return recurso->id == simboloRecurso;
	}

	recurso_t *recursoLiberado = list_find(recursosLiberados,
			(void *) _is_recurso);

	if (recursoLiberado != NULL ) {
		recursoLiberado->quantity--;
		personaje_desbloqueado_t *personajeDesbloqueado = malloc(
				sizeof(personaje_desbloqueado_t));
		personajeDesbloqueado->idRecurso = recursoLiberado->id;
		personajeDesbloqueado->idPersonaje = unPersonaje->simbolo;
		list_add(recursosUsados, personajeDesbloqueado);

		if (recursoLiberado->quantity == 0) {
			list_remove_by_condition(recursosLiberados, (void *) _is_recurso);
			recurso_destroy(recursoLiberado);
		}
	}

	return recursoLiberado != NULL ;
}

void informarDesbloqueo(personaje_bloqueado_t *unPersonaje,
		datos_planificador_t *unPlanificador) {
	header_t header;
	header.type = RESPUESTA_PEDIDO_RECURSO;
	header.length = strlen("yes") + 1;
	int bytes = sockets_send(unPersonaje->sockfd, &header, "yes");

	if (bytes == 0) {
		log_error(logFile, "Personaje %c desconectado imprevistamente.",
				unPersonaje->simbolo);
		pthread_mutex_lock(unPlanificador->mutexColasCompartidas);
		rutinaPersonajeDesconectado(unPersonaje->sockfd, unPlanificador);
		pthread_mutex_unlock(unPlanificador->mutexColasCompartidas);
	}
}

//TODO: colas compartidas. Ojo si se la llama de otro lado.
void rutinaPersonajeDesconectado(int sockfdPersonaje,
		datos_planificador_t *unPlanificador) {
	int _is_personaje(personaje_bloqueado_t *personaje) {
		return personaje->sockfd == sockfdPersonaje;
	}

	t_list *personajesListos = unPlanificador->personajesListos->elements;
	t_list *personajesBloqueados =
			unPlanificador->personajesBloqueados->elements;

	personaje_bloqueado_t *personajeDesconectado = list_remove_by_condition(
			personajesListos, (void *) _is_personaje);

	if (personajeDesconectado == NULL )
		personajeDesconectado = list_remove_by_condition(personajesBloqueados,
				(void *) _is_personaje);

	FD_CLR(personajeDesconectado->sockfd, unPlanificador->masterfds);
	close(personajeDesconectado->sockfd);
	free(personajeDesconectado);
}

recurso_t *crearRecursoUsado(recurso_t *recursoLiberado) {
	recurso_t *nuevoRecurso = malloc(sizeof(recurso_t));
	nuevoRecurso->id = recursoLiberado->id;
	nuevoRecurso->quantity = 0;

	return nuevoRecurso;
}

int aceptarConexionNueva(int newfd, fd_set *lista) {
	header_t h;
	int nbytes;

	if ((nbytes = recv(newfd, &h, sizeof(h), MSG_WAITALL)) <= 0) {
		if (nbytes == 0) //conexion cerrada
			printf("Select: socket %d desconectado.\n", newfd);
		else
			log_error(logFile, "receive");
		close(newfd);
		FD_CLR(newfd, lista);
	} else {
		switch (h.type) {
		case HANDSHAKE_PERSONAJE:
			nbytes = enviarHandshake(newfd, HANDSHAKE_ORQUESTADOR);
			if (nbytes == -1)
				log_error(logFile, "Envio handshake a Personaje.");
			responder_personaje(newfd);
			close(newfd);
			newfd = 0;
			break;
		case HANDSHAKE_NIVEL:
			nbytes = enviarHandshake(newfd, HANDSHAKE_ORQUESTADOR);
			if (nbytes == -1)
				log_error(logFile, "Envio handshake a Nivel.");
			responder_nivel(newfd);
			FD_SET(newfd, lista);
			break;
		}
	}

	return newfd;
}

void responder_personaje(int sockfd) {
	header_t header;
	char *nombreNivel;

	recv(sockfd, &header, sizeof(header), MSG_WAITALL);
	switch (header.type) {
	case PEDIDO_INFO_CONEXION:
		nombreNivel = malloc(header.length);
		recv(sockfd, nombreNivel, header.length, MSG_WAITALL);

		if (dictionary_has_key(dic_conexiones, nombreNivel)) {
			nivel_info_t *dataConexion = dictionary_get(dic_conexiones,
					nombreNivel);
			enviarDatosDePlanificadorNivel(sockfd, dataConexion);
		}

		free(nombreNivel);
		break;
	case NOTIFICACION_FIN_PLAN_NIVELES:
		chequearUltimoPersonaje();
		break;
	}
}

void chequearUltimoPersonaje(void) {
	int i = 0, hayPersonajesActivos = 1, planificadoresChequeados = 0;
	char *key;
	char c;

	while (planificadoresChequeados < dictionary_size(dic_planificadores)) {
		key = getPlanificadorKey(i);

		if (dictionary_has_key(dic_planificadores, key)) {
			datos_planificador_t *unPlanificador = dictionary_get(
					dic_planificadores, key);
			hayPersonajesActivos = tienePersonajesActivos(unPlanificador);

			if (hayPersonajesActivos) {
				free(key);
				break;
			}

			planificadoresChequeados++;
		}

		free(key);
		i++;
	}

if (!hayPersonajesActivos) {

		printf("Desea enfrentar a Koopa? y/n");

		if ((c = getchar()) == 'y') {
			char *sols[] = { "koopa", configObj->solicitudesKoopa, (char *) 0 };
			execv(configObj->binarioKoopa, sols);
		}
	}
}

int tienePersonajesActivos(datos_planificador_t *unPlanificador) {
	int personajesActivos = 1;

	pthread_mutex_lock(unPlanificador->mutexColasCompartidas);
	if (queue_is_empty(unPlanificador->personajesListos)
			&& queue_is_empty(unPlanificador->personajesBloqueados)) {
		personajesActivos = 0;
	}
	pthread_mutex_unlock(unPlanificador->mutexColasCompartidas);

	return personajesActivos;
}

void enviarDatosDePlanificadorNivel(int sockfd, nivel_info_t *dataConexion) {
	int16_t nivelLength;
	header_t header;

	char *ipNivelSerialized = ipInfo_serializer(dataConexion->addrNivel,
			&nivelLength);
	char *ipPlanSerialized = ipInfo_serializer(dataConexion->addrPlanificador,
			&header.length);
	char *payload = malloc(header.length + nivelLength);
	memcpy(payload, ipNivelSerialized, nivelLength);
	memcpy(payload + nivelLength, ipPlanSerialized, header.length);
	header.length += nivelLength;
	header.type = RESPUESTA_PEDIDO_INFO_CONEXION;
	sockets_send(sockfd, &header, payload);
	free(ipPlanSerialized);
	free(ipNivelSerialized);
	free(payload);
}

void responder_nivel(int sockfd) {
	ip_info_t *ip_nivel = malloc(sizeof(ip_info_t));
	char *nombreNivel, *data;
	header_t header;
	nivel_info_t *conexion = malloc(sizeof(nivel_info_t));
	datos_planificador_t *nuevoPlanificador;

	recv(sockfd, &header, sizeof(header), MSG_WAITALL);
	if (header.type == NOTIFICACION_DATOS_NIVEL) {
		data = malloc(header.length);
		recv(sockfd, data, header.length, MSG_WAITALL);
		ip_nivel = nivelInfo_deserializer(data, &nombreNivel);
		conexion->addrNivel = ip_nivel;
		conexion->sockfd = sockfd;
		//crear hilo planificador y enviar datos de conexion
		nuevoPlanificador = crearHiloPlanificador(nombreNivel, sockfd);
		conexion->addrPlanificador = nuevoPlanificador->ipInfo;
		dictionary_put(dic_conexiones, nombreNivel, conexion);
		enviarDatosDePlanificador(sockfd, nuevoPlanificador->ipInfo);
		free(data);
	}
}

char *getPlanificadorKey(int nivelSockfd) {
	char *key = malloc(sizeof(int));
	sprintf(key, "%d", nivelSockfd);

	return key;
}

datos_planificador_t *crearHiloPlanificador(char *nombreNivel, int nivelSockfd) {
	datos_planificador_t *newPlanificador = getDatosPlanificador(nombreNivel);

	pthread_create(newPlanificador->hPlanificador, NULL, (void*) planificador,
			(void*) newPlanificador);

	char *key = getPlanificadorKey(nivelSockfd);
	dictionary_put(dic_planificadores, key, newPlanificador);

	return newPlanificador;
}

datos_planificador_t *getDatosPlanificador(char *nombreNivel) {
	datos_planificador_t *newPlanificador = malloc(
			sizeof(datos_planificador_t));
	newPlanificador->hPlanificador = malloc(sizeof(pthread_t));
	ip_info_t *infoConexion = malloc(sizeof(ip_info_t));
	char *nombrePlanificador = malloc(strlen("Pl_") + strlen(nombreNivel) + 2);
	strcpy(nombrePlanificador, "Pl_");
	strcat(nombrePlanificador, nombreNivel);
	newPlanificador->nombre = nombrePlanificador;
	int sockfd = sockets_createServer(configObj->ipInfo->addr, "0",
			configObj->bufferconexiones);
	if (getIpInfo(sockfd, infoConexion) == -1) {
		log_error(logFile, "planificador");
		printf("fallo obtener datos planificador\n");
	}
	newPlanificador->ipInfo = infoConexion;
	newPlanificador->socket = sockfd;
	newPlanificador->personajesBloqueados = queue_create();
	newPlanificador->personajesListos = queue_create();
	newPlanificador->masterfds = malloc(sizeof(fd_set));
	newPlanificador->mutexColasCompartidas = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(newPlanificador->mutexColasCompartidas, NULL );
	FD_ZERO(newPlanificador->masterfds);

	return newPlanificador;
}

void enviarDatosDePlanificador(int sockfd, ip_info_t *ipInfo) {
	header_t header;

	header.type = NOTIFICACION_DATOS_PLANIFICADOR;
	char *ipInfoSerialized = ipInfo_serializer(ipInfo, &header.length);
	sockets_send(sockfd, &header, ipInfoSerialized);
	free(ipInfoSerialized);
}

ip_info_t *nivelInfo_deserializer(char *serialized, char **nombre) {
	ip_info_t *ipInfo = malloc(sizeof(ip_info_t));
	int offset = 0, tmp_size = 0;

	for (tmp_size = 1; serialized[tmp_size - 1] != '\0'; tmp_size++)
		;
	char *nom = malloc(tmp_size);
	memcpy(nom, serialized, tmp_size);
	printf("%s\n", nom);

	*nombre = nom;
	offset = tmp_size;

	for (tmp_size = 1; (serialized + offset)[tmp_size - 1] != '\0'; tmp_size++)
		;
	ipInfo->addr = malloc(tmp_size);
	memcpy(ipInfo->addr, serialized + offset, tmp_size);
	offset += tmp_size;

	for (tmp_size = 1; (serialized + offset)[tmp_size - 1] != '\0'; tmp_size++)
		;
	ipInfo->port = malloc(tmp_size);
	memcpy(ipInfo->port, serialized + offset, tmp_size);
	offset += tmp_size;

	return ipInfo;
}

void orquestador(void) {

	int fdMax = 0, sockfd, newfd, rv = 0;
	int serverSocket = sockets_createServer(configObj->ipInfo->addr,
			configObj->ipInfo->port, configObj->bufferconexiones);
	logFile = log_create(LOG_PATH, "HiloOrquestador", false,
			log_level_from_string(configObj->detalleLog));
	fd_set readfds;
	fd_set master;
	dic_conexiones = dictionary_create();
	dic_planificadores = dictionary_create();
	FD_ZERO(&readfds);
	FD_ZERO(&master);
	FD_SET(serverSocket, &master);
	fdMax = serverSocket;
	log_info(logFile, "Esperando conexiones en ip: %s port: %s\n",
			configObj->ipInfo->addr, configObj->ipInfo->port);

	while (1) {
		readfds = master;
		rv = select(fdMax + 1, &readfds, NULL, NULL, NULL );
		if (rv < 0) {
			log_error(logFile, "select");
		} else if (rv == 0) {
			log_error(logFile, "select time out connection");
		} else {
			for (sockfd = 0; sockfd <= fdMax; sockfd++) {
				if (FD_ISSET(sockfd, &readfds)) {
					if (sockfd == serverSocket) {
						if ((newfd = sockets_accept(serverSocket)) == -1)
							log_error(logFile, "accept");
						else {
							newfd = aceptarConexionNueva(newfd, &master);
							if (newfd > fdMax)
								fdMax = newfd;
						}
					} else {
						if (atenderPedido(sockfd) == 0) {
							FD_CLR(sockfd, &master);
							close(sockfd);
						}
					}
				}
			}
		}
	}
}

int getIpInfo(int sockfd, ip_info_t* ipInfo) {
	struct sockaddr_in sockaddr;

	socklen_t addrlen = sizeof(sockaddr);
	if (getsockname(sockfd, (struct sockaddr *) &sockaddr, &addrlen) == -1)
		return -1;
	ipInfo->port = malloc(sizeof(int));
	sprintf(ipInfo->port, "%d", ntohs(sockaddr.sin_port));
	ipInfo->addr = inet_ntoa(sockaddr.sin_addr);
	return 0;
}

void getConfiguracion(void) {
	configObj = malloc(sizeof(PLATAFORMA_CONF));
	configObj->ipInfo = malloc(sizeof(ip_info_t));
	t_config *conf = config_create(CONF_PATH);

	if (config_has_property(conf, "IPORQUESTADOR")) {
		configObj->ipInfo->addr = malloc(
				strlen(config_get_string_value(conf, "IPORQUESTADOR")) + 1);
		strcpy(configObj->ipInfo->addr,
				config_get_string_value(conf, "IPORQUESTADOR"));
	}

	if (config_has_property(conf, "PUERTOORQUESTADOR")) {
		configObj->ipInfo->port = malloc(
				strlen(config_get_string_value(conf, "PUERTOORQUESTADOR")) + 1);
		strcpy(configObj->ipInfo->port,
				config_get_string_value(conf, "PUERTOORQUESTADOR"));
	}

	if (config_has_property(conf, "bufferconexiones"))
		configObj->bufferconexiones = config_get_int_value(conf,
				"bufferconexiones");
	if (config_has_property(conf, "detallelog")) {
		configObj->detalleLog = malloc(
				strlen(config_get_string_value(conf, "detallelog")) + 1);
		strcpy(configObj->detalleLog,
				config_get_string_value(conf, "detallelog"));
	}
	if (config_has_property(conf, "QUANTUM")) {
		configObj->quantum = config_get_int_value(conf, "QUANTUM");
	}
	if (config_has_property(conf, "WAIT")) {
		configObj->wait = config_get_double_value(conf, "WAIT");
	}
	if (config_has_property(conf, "solicitudesKoopa")) {
		configObj->solicitudesKoopa = malloc(
				strlen(config_get_string_value(conf, "solicitudesKoopa")) + 1);
		strcpy(configObj->solicitudesKoopa,
				config_get_string_value(conf, "solicitudesKoopa"));
	}
	if (config_has_property(conf, "binarioKoopa")) {
		configObj->binarioKoopa = malloc(
				strlen(config_get_string_value(conf, "binarioKoopa")) + 1);
		strcpy(configObj->binarioKoopa,
				config_get_string_value(conf, "binarioKoopa"));
	}
	config_destroy(conf);
}

void configObj_destroy(PLATAFORMA_CONF *configObj) {
	free(configObj->ipInfo);
	free(configObj->detalleLog);
	free(configObj);
}
