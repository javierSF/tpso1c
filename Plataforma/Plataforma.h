/*
 * Plataforma.h
 *
 *  Created on: 23/04/2013
 *      Author: utnso
 */

#ifndef PLATAFORMA_H_
#define PLATAFORMA_H_

#include <stdlib.h>
#include <stdio.h>
#include <commons/config.h>
#include <commons/log.h>
#include <commons/sockets.h>
#include <commons/string.h>
#include <commons/collections/queue.h>
#include <commons/collections/list.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/inotify.h>

#define LOG_PATH "./txt/log.txt"
#define CONF_PATH "./txt/config.txt"
#define BUFF_SIZE 1024
#define MAXDATASIZE 100
#define MAXCONN 5
#define direccion INADDR_ANY
#define EVENT_SIZE  ( sizeof (struct inotify_event) + 24 )
#define BUF_LEN     ( 1024 * EVENT_SIZE )

typedef struct {
	ip_info_t *ipInfo;
	char *detalleLog;
	int bufferconexiones;
	int quantum;
	double wait;
	char *solicitudesKoopa;
	char *binarioKoopa;
} PLATAFORMA_CONF;

typedef struct {
	char* nombre;
	ip_info_t* addrNivel;
	ip_info_t* addrPlanificador;
	int sockfd;
} nivel_info_t;

typedef struct {
	ip_info_t *nivel;
	ip_info_t *planificador;
} conexion_nivel_plan_t;

typedef struct {
	pthread_t *hPlanificador;
	t_queue *personajesListos;
	t_queue *personajesBloqueados;
	pthread_mutex_t *mutexColasCompartidas;
	fd_set *masterfds;
	ip_info_t *ipInfo;
	int socket;
	char *nombre;
} datos_planificador_t;

typedef struct {
	char simbolo;
	char recursoPedido;
	int sockfd;
} personaje_bloqueado_t;

t_log *logFile;
PLATAFORMA_CONF *configObj;
t_dictionary *dic_conexiones, *dic_planificadores;
void orquestador(void);
void planificador(datos_planificador_t *datos);
void getConfiguracion(void);
void configObj_destroy(PLATAFORMA_CONF *configObj);
ip_info_t *nivelInfo_deserializer(char *serialized, char **nombre);
void responder_personaje(int sockfd);
void responder_nivel(int sockfd);
int aceptarConexionNueva(int sockfd, fd_set *lista);
int atenderPedido(int sockfd);
void atenderRecursosLiberados(int sockfd, t_list *recursosLiberados);
void enviarHandshakeOrquestador(int sockfd);
datos_planificador_t *crearHiloPlanificador(char *nombreNivel, int nivelSockfd);
int getIpInfo(int sockfd, ip_info_t* ipInfo);
void enviarDatosDePlanificador(int sockfd, ip_info_t *ipInfo);
void enviarDatosDePlanificadorNivel(int sockfd, nivel_info_t *ipInfo);
t_list *desbloquearPersonajes(int nivelSockfd, t_list *listaRecursosLiberados);
recurso_t *crearRecursoUsado(recurso_t *recursoLiberado);
int usarRecurso(char simboloRecurso, t_list *recursosLiberados,
		t_list *recursosUsados, personaje_bloqueado_t *unPersonaje);
void informarDesbloqueo(personaje_bloqueado_t *unPersonaje,
		datos_planificador_t *unPlanificador);
void notificarRecursosUsados(int sockfd, t_list *recursosUsados);
void hacerRecovery(t_list *personajesInterbloqueados, int nivelSockfd);
void rutinaPersonajeDesconectado(int sockfdPersonaje,
		datos_planificador_t *unPlanificador);
datos_planificador_t *getDatosPlanificador(char *nombreNivel);
personaje_bloqueado_t* buscarPrimerPersonajeConectado(t_list* personajes,
		datos_planificador_t* planificador);
void desbloquearPersonaje(personaje_bloqueado_t *unPersonaje,
		datos_planificador_t *unPlanificador);
void enviarNotificacionMuerte(personaje_bloqueado_t* unPersonaje);
datos_planificador_t *findPlanificador(int nivelSockfd);
void chequearUltimoPersonaje(void);
char *getPlanificadorKey(int nivelSockfd);
int tienePersonajesActivos(datos_planificador_t *unPlanificador);
void matarHilo(int sockfd);

#endif /* PLATAFORMA_H_ */
