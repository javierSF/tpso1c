/*
 * Planificador.h
 *
 *  Created on: 20/06/2013
 *      Author: utnso
 */

#ifndef PLANIFICADOR_H_
#define PLANIFICADOR_H_

#include "Plataforma.h"

void enviarMensajeDeMovimiento(personaje_bloqueado_t* personajeEnMovimiento);
int evaluarRespuestaMovPersonaje(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, fd_set* masterEscuchafds);
personaje_bloqueado_t* obtenerProximoPersonaje(datos_planificador_t* datos);
int aceptarConexion(int listener, datos_planificador_t *datos);
int aceptarNuevoPersonaje(int persSockfd, datos_planificador_t* datos);
void aceptarNivel(int nivSockfd);
int enviarHandshakePlanificador(int sockfd);
personaje_bloqueado_t* crearPersonaje(char idP, char idR, int sockfd);
void destroyPersonaje(personaje_bloqueado_t* personaje);
personaje_bloqueado_t* recibirInfoPersonaje(int newsock);
int selectTimeOut(double segundos, int fdmax, fd_set* escuchafds);
int moverPersonaje(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, fd_set* masterEscuchafds);
int tratarNotificacionBloqueo(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, header_t header);
int finalizarPersonaje(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, header_t header,  fd_set* masterEscuchafds);
int tratarNotificacionMovimento(personaje_bloqueado_t* personajeEnMovimiento,
		datos_planificador_t* datos, header_t header);
void sacarPersonajeCola(personaje_bloqueado_t* personaje, t_queue* cola);
int _is_personaje(personaje_bloqueado_t *p);
void logDeListas(personaje_bloqueado_t* personajeEnMovimiento, t_queue* listos,
		t_queue* bloqueados);
int getNotifyFileDescriptor();
void tratarModificacion(int file_descriptor);
void actualizarQuantum(void);
void ejecutarKoopa(char *solicitudes);
void tratarPedido(int sockfd, datos_planificador_t *datos, fd_set *master);
void tratarReinicioNivel(int sockfd, datos_planificador_t *datos,
		fd_set *master);

#endif /* PLANIFICADOR_H_ */
