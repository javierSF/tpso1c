/*
 * Nivel.c
 *
 *  Created on: 25/04/2013
 *      Author: utnso
 */

#include "Nivel.h"

int main(void) {

	// CONFIGURACION PROPIA DEL NIVEL
	configObj = getNivelConfig();
	logFile = log_create(LOG_PATH, "ProcesoNivel", false,
	log_level_from_string(configObj->logLevel));
	separador_log(configObj->nombre);
	pthread_t hdeadlock;
	pthread_create(&hdeadlock, NULL, (void*) gestionarDeadlock, NULL );
	inicializarInterfazGrafica();
	levantarCajasConfiguracion();
	nivel_gui_dibujar(listaRecursos);
	setSockEscucha();
	fd_set readfds;
	fd_set master;
	FD_ZERO(&readfds);
	FD_ZERO(&master);
	FD_SET(sockEscucha, &master);
	int fdMax = sockEscucha, rv, newsock;
	// CONEXIONES

	conectarAProcesos();
	log_info(logFile, "Esperando conexiones en ip: %s port: %s",
			configObj->localhostaddr, configObj->localhostport);
	while (1) {
		readfds = master;
		rv = select(fdMax + 1, &readfds, NULL, NULL, NULL );
		if (rv < 0) {
			log_error(logFile, "select");
		} else if (rv == 0) {
			log_error(logFile, "select time out connection");
		} else {
			if (FD_ISSET(sockEscucha, &readfds)) {
				newsock = conectarNuevoPersonaje(&readfds);
				FD_SET(newsock, &master);
				if (fdMax < newsock)
					fdMax = newsock;
			} else {
				buscarPersonajeConectado(&readfds, &master);
			}
		}
	}
	nivel_gui_terminar();
	return EXIT_SUCCESS;
}

int aceptarConexion(int sockfd) {
	struct sockaddr_in their_addr;
	int sin_size, new_fd;

	sin_size = sizeof(struct sockaddr_in);
	new_fd = accept(sockfd, (struct sockaddr *) &their_addr,
			(socklen_t *) &sin_size);

	if (new_fd == -1) {
		log_error(logFile, "accept");
		exit(EXIT_FAILURE);
	} else {
		log_info(logFile, "Conexion establecida con %s",
				inet_ntoa(their_addr.sin_addr));
	}

	printf("server: got connection from %s\n", inet_ntoa(their_addr.sin_addr));

	return new_fd;
}

void escucharSocket(int sockfd) {
	int error = listen(sockfd, 1);

	if (error == -1) {
		log_error(logFile, "listen");
		exit(EXIT_FAILURE);
	}
}

void conectarAProcesos() {

	orquestadorSockfd = connectOrquestador();
	hacerHandshake(orquestadorSockfd);
	enviarDatosConexion();
	recibirDatosPlanificador();

}

void hacerHandshake(int sockfd) {
	header_t header;
	header.type = HANDSHAKE_NIVEL;
	header.length = 0;
	sockets_send(sockfd, &header, '\0');
	if (recv(sockfd, &header, sizeof(header), MSG_WAITALL) == 0) {
		log_error(logFile, "Conexion al tratar de hacer handshake.");
	}

	switch (header.type) {
	case HANDSHAKE_PLANIFICADOR:
		log_info(logFile, "Handshake Planificador.");
		break;
	case HANDSHAKE_ORQUESTADOR:
		log_info(logFile, "Handshake Orquestador.");
		break;
	case HANDSHAKE_PERSONAJE:
		log_info(logFile, "Handshake Personaje.");
		break;
	default:
		log_error(logFile, "Handshake no reconocido.");
	}
}

void recibirHandshake(int sockfd) {
	header_t header;
	if (recv(sockfd, &header, sizeof(header), MSG_WAITALL) == 0) {
		log_error(logFile, "Conexion al tratar de recibir handshake.\n");
	}

	switch (header.type) {
	case HANDSHAKE_PLANIFICADOR:
		log_info(logFile, "Handshake Planificador recibido.");
		break;
	case HANDSHAKE_ORQUESTADOR:
		log_info(logFile, "Handshake Orquestador recibido.");
		break;
	case HANDSHAKE_PERSONAJE:
		log_info(logFile, "Handshake Personaje recibido.");
		break;
	default:
		log_error(logFile, "Handshake recibido no reconocido.\n");
	}

	header.type = HANDSHAKE_NIVEL;
	header.length = 0;
	sockets_send(sockfd, &header, '\0');

}

void enviarDatosConexion() {
	header_t h;
	ip_info_t i;
	i.addr = configObj->localhostaddr;
	i.port = configObj->localhostport;
	h.type = NOTIFICACION_DATOS_NIVEL;
	h.length = strlen(configObj->nombre) + 1;

	int16_t length;

	char* ipInfo = ipInfo_serializer(&i, &length);

	char* data = malloc(h.length + length);
	memcpy(data, configObj->nombre, h.length);
	memcpy(data + h.length, ipInfo, length);
	h.length += length;
	sockets_send(orquestadorSockfd, &h, data);
	free(data);
}
//----------------------------------------------------------------------------------------------
//CONEXIONES PERSONAJES
//----------------------------------------------------------------------------------------------

int conectarNuevoPersonaje(fd_set *listaS) {
	log_info(logFile, "Conectando nuevo personaje...");
	int newsock = 0;
	newsock = sockets_accept(sockEscucha);
	recibirHandshake(newsock);
	header_t header;
	if (recv(newsock, &header, sizeof(header), MSG_WAITALL) == 0) {
		log_error(logFile, "Perdida de conexion al conectar un personaje.");
	} else {
		log_info(logFile, "Recibiendo datos personaje...");
	}

	char* data;

	if (header.type == NOTIFICACION_DATOS_PERSONAJE) {
		data = malloc(header.length);
		recv(newsock, data, header.length, MSG_WAITALL);
		crearPersonajeIngresado(data[0], 5, newsock);
	} else {
		log_error(logFile, "Mensaje inesperado de personaje, protocolo: %d",
				header.type);
	}
	if (newsock == -1) {
		log_error(logFile, "Conexion con personaje, file descriptor error");
		exit(EXIT_FAILURE);
	} else {
		log_info(logFile, "Conectado con personaje: %c, Socket: %d", data[0],
				newsock);
	}
	//close(newsock); deberia hacerse al morir un personaje
	free(data);
	return newsock;
}

void buscarPersonajeConectado(fd_set *readfds, fd_set *masterfds) {
	log_info(logFile, "Buscando personaje...");
	ITEM_NIVEL* tempP = listaPersonajes;
	while (tempP != NULL ) {
		if (FD_ISSET(tempP->socket, readfds)) {
			log_info(logFile, "Personaje: %c, encontrado", tempP->id);
			atenderPersonaje(tempP->id, tempP->socket, masterfds);
			break;
		}
		tempP = tempP->next;
	}
	if (tempP == NULL ) {
		log_error(logFile, "Socket de personaje no encontrado en readfds");
	}
}

void atenderPersonaje(char pId, int sockfd, fd_set* masterfds) {
	header_t h;
	int nbytes;
	if ((nbytes = recv(sockfd, &h, sizeof(h), MSG_WAITALL)) <= 0) {
		if (nbytes == 0){ //conexion cerrada
			log_warning(logFile, "Personaje %c socket %d desconectado inesperadamente",pId, sockfd);
			atenderNotificacionNivelFinalizado(pId, sockfd, masterfds);
		}else
			log_error(logFile, "receive");
	} else {
		char* data = malloc(h.length);
		switch (h.type) {
		case MOVIMIENTO:
			recv(sockfd, data, h.length, MSG_WAITALL);
			atenderMovimiento(pId, data);
			break;
		case PEDIDO_RECURSO:
			recv(sockfd, data, h.length, MSG_WAITALL);
			atenderPedidoRecurso(pId, data, sockfd);
			break;
		case PEDIDO_INFO_ITEM:
			recv(sockfd, data, h.length, MSG_WAITALL);
			atenderPedidoInfoItem(data, sockfd);
			break;
		case NOTIFICACION_NIVEL_FINALIZADO:
			recv(sockfd, data, h.length, MSG_WAITALL);
			atenderNotificacionNivelFinalizado(pId, sockfd, masterfds);
			break;
		case NOTIFICACION_MUERTE:
			recv(sockfd, data, h.length, MSG_WAITALL);
			atenderNotificacionNivelFinalizado(pId, sockfd, masterfds);
			break;
		default:
			log_error(logFile,
					"Protocolo invalido (%d) para comunicarse con el nivel",
					h.type);
			break;
		}
		free(data);
	}
}

void atenderMovimiento(char pId, char* data) {
	indicaciones_t* ind;
	ind = indicaciones_deserializer(data);
	actualizar_posicion(pId, ind);
	dibujarPersonajes();
	indicaciones_destroy(ind);
}

void atenderPedidoRecurso(char pId, char* data, int sockfd) {
	char rId = data[0];
	int respuesta;
	if (personajeEnCaja(pId, rId)) {
		respuesta = darRecursoPersonaje(&listaPersonajes, &listaRecursos, pId,
				rId);
	} else {
		log_error(logFile, "acceso incorrecto a caja de recurso: %s", rId);
	}
	if (respuesta) {
		log_info(logFile, "Se le asigno el recurso: %c, al personaje: %c", rId,
				pId);
		notificacionDarRecurso(sockfd);
	} else {
		log_info(logFile, "Se bloqueo el personaje: %c, al pedir el recruso %c",
				pId, rId);
		notificacionBloqueo(sockfd);
	}
}

void atenderPedidoInfoItem(char* data, int sockfd) {

	char rId = data[0];
	log_info(logFile, "Atendiendo pedido de posicion de caja %c", rId);
	coordenadas_t* coord = obtenerCoordenadas(listaRecursos, rId);
	header_t h;
	int16_t length;
	char* coordSerialized = coordenadas_serializer(coord, &length);
	h.type = RESPUESTA_INFO_ITEM;
	h.length = length;
	sockets_send(sockfd, &h, coordSerialized);

	log_info(logFile, "Enviando posicion de caja %c (%d,%d)", rId, coord->ejeX,
			coord->ejeY);
	free(coordSerialized);
	coordenadas_destroy(coord);

}

void notificacionBloqueo(int personajeSockfd) {
	header_t header;
	header.type = RESPUESTA_PEDIDO_RECURSO;
	header.length = strlen("no") + 1;
	sockets_send(personajeSockfd, &header, "no");

}

void notificacionDarRecurso(int personajeSockfd) {
	header_t header;
	header.type = RESPUESTA_PEDIDO_RECURSO;
	header.length = strlen("yes") + 1;
	sockets_send(personajeSockfd, &header, "yes");

}

void atenderNotificacionNivelFinalizado(char pId, int sockfd, fd_set* masterfds) {
	log_info(logFile, "Finalizando nivel...");
	t_list* adquiridos = NULL;
	t_list* asignados = NULL;
	log_debug(logFile, "Obteniendo recursos del personaje");
	adquiridos = getObjetosAdquiridosSerializable(listaPersonajes, pId);
	log_debug(logFile, "Sacando personaje de la bolsa");
	FD_CLR(sockfd, masterfds);
	log_debug(logFile, "Matando personaje");
	matarPersonaje(&listaPersonajes, &listaRecursos, pId);
	mandarLiberadosAOrquestador(adquiridos);
	asignados = esperarAsignadosPorOrquestador();
	actualizarEstado(asignados);
	listaRecursos_destroy(adquiridos);
	listaRecursos_destroy(asignados);
	log_info(logFile, "El personaje: %c, finalizo el nivel", pId);
	dibujarPersonajes();
}

//----------------------------------------------------------------------------------------------
//CONEXIONES ORQUESTADOR
//----------------------------------------------------------------------------------------------

int connectOrquestador() {
	int orquestadorSockfd = sockets_createClient(configObj->orquestadoraddr,
			configObj->orquestadorport);

	if (orquestadorSockfd == -1) {
		log_error(logFile, "Conexion con orquestador fallo");
		exit(EXIT_FAILURE);
	} else {
		log_info(logFile, "Conectado con orquestador, ip: %s, port: %s",
				configObj->orquestadoraddr, configObj->orquestadorport);
	}

	return orquestadorSockfd;
}

//DEPRECATED?
void recibirDatosPlanificador() {
	header_t header;
	char* data;
	if (recv(orquestadorSockfd, &header, sizeof(header), MSG_WAITALL) == 0) {
		log_error(logFile, "Conexion perdida con el planificador");
	}

	if (header.type == NOTIFICACION_DATOS_PLANIFICADOR) {
		data = malloc(header.length);
		recv(orquestadorSockfd, data, header.length, MSG_WAITALL);
		ip_info_t *infoPlanificador = ipInfo_deserializer(data);
		log_info(logFile, "Planificador IP: %s port: %s",
				infoPlanificador->addr, infoPlanificador->port);
	}
	free(data);
}

void mandarLiberadosAOrquestador(t_list* recursosLiberados) {
	header_t header;
	header.type = NOTIFICACION_RECURSOS_LIBERADOS;
	int16_t length;
	char* data = listaRecursos_serializer(recursosLiberados, &length);
	header.length = length;

	if (sockets_send(orquestadorSockfd, &header, data) == 0) {

		log_error(logFile, "Se perdio la conexion con orquestador");
	} else {
		log_info(logFile, "Enviando recuros liberados al planificador");
	}
	free(data);
}

t_list* esperarAsignadosPorOrquestador() {
	log_info(logFile, "Reciviendo recursos asignados");
	header_t header;
	t_list* asignados = NULL;
	char* data;
	if (recv(orquestadorSockfd, &header, sizeof(header), MSG_WAITALL) == 0) {
		log_error(logFile, "Conexion perdida con el orqestador");
	}

	if (header.type == NOTIFICACION_RECURSOS_ASIGNADOS) {
		data = malloc(header.length);
		if (header.length > 0) {
			recv(orquestadorSockfd, data, header.length, MSG_WAITALL);
			asignados = listaRecursos_deserializer(data, header.length);
		} else if (header.length == 0) {
			log_debug(logFile, "El orquestador no uso ningun recurso");
			return asignados = list_create();
		}

	} else {
		log_error(logFile, "Mensaje inesperado del Orquestador");
	}
	free(data);
	return asignados;
}

void notificarDeadlock(t_list* bloqueados) {
	log_debug(logFile, "Notificando existencia de Deadlock");
	header_t header;
	header.type = NOTIFICACION_PERSONAJES_ITERBLOQUEADOS;
	int16_t length;
	char* data = personajesInterbloqueados_serializer(bloqueados, &length);
	header.length = length;
	if (sockets_send(orquestadorSockfd, &header, data) == 0 ){
		log_error(logFile, "No se pudo enviar notificacion deadlock");
	}
	free(data);
}

//----------------------------------------------------------------------------------------------
// FUNCIONALIDAD DEL NIVEL
//----------------------------------------------------------------------------------------------

void crearPersonajeIngresado(char id, int vidas, int socket) {
	log_info(logFile, "Creando Personaje...");
	CrearPersonaje(&listaPersonajes, id, 0, 0, vidas, socket);
	dibujarPersonajes();

}

void levantarCajasConfiguracion() {
	t_config* configFile = config_create(CONFIG_PATH);
	char s[20], t[20];
	char*u = "Caja1";
	int i = 1;

	while (config_get_string_value(configFile, u) != NULL ) {

		strcpy(s, "Caja");
		sprintf(t, "%d", i);
		u = strcat(s, t);
		if (config_get_string_value(configFile, u) != NULL ) {
			char **array_values = string_split(
					config_get_string_value(configFile, u), ",");
			crearCajasConVector(array_values);
		} else {
			log_warning(logFile, "No se pudo levantar la caja: %s", u);
		}
		i++;
	};
	config_destroy(configFile);
}

void crearCajasConVector(char** array) {
	char id = array[1][0];
	int instancia = atoi(array[2]);
	int pos_x = atoi(array[3]);
	int pos_y = atoi(array[4]);

	if (pos_x <= col && pos_y <= fil) {
		CrearCaja(&listaRecursos, id, pos_x, pos_y, instancia);
		log_info(logFile, "Se creo la caja:%c, cantidad:%d, Pos:(%d,%d)", id,
				instancia, pos_x, pos_y);
	} else {
		log_warning(logFile,
				"No se creo la caja:%c, cantidad:%d, Pos:(%d,%d), sobrepasa los limites",
				id, instancia, pos_x, pos_y);
	}
}

NIVEL_CONF* getNivelConfig() {
	t_config* conF = config_create(CONFIG_PATH);
	NIVEL_CONF *configObj = malloc(sizeof(NIVEL_CONF));

	if (config_has_property(conF, "Nombre")) {
		strcpy(configObj->nombre, config_get_string_value(conF, "Nombre"));
	}
	if (config_has_property(conF, "TiempoChequeoDeadlock")) {
		configObj->deadlockTime = config_get_int_value(conF,
				"TiempoChequeoDeadlock");
	}
	if (config_has_property(conF, "Recovery")) {
		configObj->recovery = config_get_int_value(conF, "Recovery");
	}
	char *tok = ":";
	if (config_has_property(conF, "orquestador")) {
		char *orqaddr = strdup(config_get_string_value(conF, "orquestador"));
		configObj->orquestadoraddr = strtok(orqaddr, tok);
		configObj->orquestadorport = strtok(NULL, tok);
	}
	if (config_has_property(conF, "localhost")) {
		char *locaddr = strdup(config_get_string_value(conF, "localhost"));
		configObj->localhostaddr = strtok(locaddr, tok);
		configObj->localhostport = strtok(NULL, tok);
	}
	if (config_has_property(conF, "Log")) {
		configObj->logLevel = malloc(
				strlen(config_get_string_value(conF, "Log")));
		strcpy(configObj->logLevel, config_get_string_value(conF, "Log"));
	}
	config_destroy(conF);
	return configObj;
}

void actualizar_posicion(char pId, indicaciones_t* ind) {
	coordenadas_t* posActual = obtenerCoordenadas(listaPersonajes, pId);
	log_info(logFile, "Personaje: %c, en (%d,%d) moviendose...", pId,
			posActual->ejeX, posActual->ejeY);
	log_info(logFile, "Intenta moverse %s, sentido %s", ind->eje, ind->sentido);

	if ((strcmp(ind->eje, EJE_X) == 0) && (strcmp(ind->sentido, POS) == 0)) {
		MoverPersonaje(listaPersonajes, pId, ++(posActual->ejeX),
				posActual->ejeY);
	}
	if ((strcmp(ind->eje, EJE_X) == 0) && (strcmp(ind->sentido, NEG) == 0)) {
		MoverPersonaje(listaPersonajes, pId, --(posActual->ejeX),
				posActual->ejeY);
	}
	if ((strcmp(ind->eje, EJE_Y) == 0) && (strcmp(ind->sentido, POS) == 0)) {

		MoverPersonaje(listaPersonajes, pId, posActual->ejeX,
				++(posActual->ejeY));
	}
	if ((strcmp(ind->eje, EJE_Y) == 0) && (strcmp(ind->sentido, NEG) == 0)) {
		MoverPersonaje(listaPersonajes, pId, posActual->ejeX,
				--(posActual->ejeY));
	}
	posActual = obtenerCoordenadas(listaPersonajes, pId);
	log_info(logFile, "Personaje: %c, se movio a (%d,%d)", pId, posActual->ejeX,
			posActual->ejeY);
	coordenadas_destroy(posActual);
}

int personajeEnCaja(char pId, char rId) {

	int enCaja = 0;
	coordenadas_t* posCaja = obtenerCoordenadas(listaRecursos, rId);
	coordenadas_t* posPersonaje = obtenerCoordenadas(listaPersonajes, pId);

	if (((posCaja->ejeX) == (posPersonaje->ejeX))
			&& (posCaja->ejeY == posPersonaje->ejeY)) {
		enCaja = 1;
	}

	coordenadas_destroy(posCaja);
	coordenadas_destroy(posPersonaje);

	return enCaja;
}

void setSockEscucha() {
	sockEscucha = sockets_createServer(configObj->localhostaddr,
			configObj->localhostport, 1);
	if (sockEscucha == -1) {
		log_error(logFile, "No se pudo crear el socket escucha");
	} else {
		log_info(logFile, "Nivel escucnando en ip: %s, puerto: %s, fd:%d",
				configObj->localhostaddr, configObj->localhostport,
				sockEscucha);
	}
}

void actualizarEstado(t_list* asignados) {

	log_debug(logFile, "Actualizando estado de recurso y personajes...");
	personaje_desbloqueado_t* personaje;
	int i;
	for (i = 0; i < asignados->elements_count; ++i) {
		personaje = list_get(asignados, i);
		darRecursoPersonaje(&listaPersonajes, &listaRecursos, personaje->idPersonaje, personaje->idRecurso);
	}

}

void gestionarDeadlock() {

	int wait = configObj->deadlockTime;
	log_debug(logFile, "DeadLock se ejecutra cada %d segundos", wait);
	while (1) {
		sleep(wait);
		log_info(logFile, "Ejecutando algoritmo de detencion de Dead Lock");
		t_list* finalizados = list_create();
		t_list* bloqueados = list_create();
		t_dl_vector* personaje;
		int i;
		finalizados = deadlockDetection(listaRecursos, listaPersonajes);
		for (i = 0; i < finalizados->elements_count; i++) {
			personaje = list_get(finalizados, i);

			if (!(personaje->value)) {
				list_add(bloqueados, crearNodoInterbloqueado(personaje->id));
				log_info(logFile, "Personaje: %c, InterBloqueado",
						personaje->id);
			}
		}
		if (bloqueados->elements_count > 1) {
			log_info(logFile, "Se detecto deadlock entre %d personajes",
					bloqueados->elements_count);
			if (configObj->recovery) {
				notificarDeadlock(bloqueados);
			}

		}
		list_destroy(finalizados);
		list_destroy(bloqueados);
	}

}

void separador_log(char* head) {
	log_info(logFile,
			"------------------------------ %s -------------------------------",
			head);
}

void inicializarInterfazGrafica() {

	if (nivel_gui_inicializar() == -1) {
		log_error(logFile, "No se pudo inicializar la interfaz grafica");
	}
	if (nivel_gui_get_area_nivel(&fil, &col) == -1) {
		log_error(logFile, "No se pudo inicializar el area del nivel");
	}

}

void dibujarPersonajes() {
	log_info(logFile, "Dibujando Personaje");
	ITEM_NIVEL* tempI = NULL;
	ITEM_NIVEL* tempR = NULL;
	ITEM_NIVEL* tempP = NULL;

	tempR = listaRecursos;
	tempP = listaPersonajes;

	while (tempR != NULL ) {
		CrearCaja(&tempI, tempR->id, tempR->posx, tempR->posy, tempR->quantity);
		tempR = tempR->next;
	}

	while (tempP != NULL ) {
		CrearPersonaje(&tempI, tempP->id, tempP->posx, tempP->posy, 1,
				tempP->socket);
		tempP = tempP->next;
	}

	if (nivel_gui_dibujar(tempI) == -1) {
		log_info(logFile, "No se puedo dibujar el personaje");
		//	EXIT_FAILURE;
	} else {
		log_info(logFile, "Personaje Dibujado");
	}
	//TODO: Destroy
}
