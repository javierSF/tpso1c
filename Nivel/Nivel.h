/*
 * Nivel.h
 *
 *  Created on: 25/04/2013
 *      Author: utnso
 */

#ifndef NIVEL_H_
#define NIVEL_H_

//------------------------------------------------------------------------------------------------
// 	INCLUDES
//------------------------------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <commons/config.h>
#include <commons/log.h>
#include <commons/collections/list.h>
#include <commons/sockets.h>
#include <commons/tad_items.h>
#include <commons/deadlock_detection.h>
#include <commons/string.h>
#include <commons/deadlock_detection.h>
#include <unistd.h>
#include <pthread.h>

//------------------------------------------------------------------------------------------------
// 	CONSTANTES
//------------------------------------------------------------------------------------------------
#define CONFIG_PATH "./nivelconfig.txt"
#define LOG_PATH "./nivellog.txt"
#define POS "+"
#define NEG "-"
#define EJE_X "ejeX"
#define EJE_Y "ejeY"

//------------------------------------------------------------------------------------------------
// 	TYPES
//------------------------------------------------------------------------------------------------
typedef struct nivel_conf_t {
	char nombre[10];
	char simbolo[2];
	char *orquestadoraddr;
	char *orquestadorport;
	char *localhostaddr;
	char *localhostport;
	char *logLevel;
	int deadlockTime;
	int recovery;
} NIVEL_CONF;

//------------------------------------------------------------------------------------------------
// 	GLOBALES
//------------------------------------------------------------------------------------------------
t_log *logFile;
//t_config* configFile;
int orquestadorSockfd;
int sockEscucha;
int fil = 100, col = 100;
//int fil, col;
ITEM_NIVEL* listaRecursos = NULL;
ITEM_NIVEL* listaPersonajes = NULL;
NIVEL_CONF *configObj;

//------------------------------------------------------------------------------------------------
// 	FIRMAS
//------------------------------------------------------------------------------------------------
int getSocket(void);
void bindSocket(int);
void escucharSocket(int);
int aceptarConexion(int);
void levantarCajasConfiguracion(); //t_config* configFile);
void crearCajasConVector(char** array);
void crearPersonajeIngresado(char id, int vidas, int socket);
NIVEL_CONF* getNivelConfig(); //t_config* configFile);
void conectarAProcesos();
int connectOrquestador();
void enviarDatosConexion();
int conectarNuevoPersonaje();
void hacerHandshake(int sock);
void recibirDatosPlanificador();
void buscarPersonajeConectado(fd_set *readfds, fd_set *masterfds);
void atenderPersonaje(char id, int sockfd, fd_set* masterfds);
void atenderMovimiento(char pId, char* data);
void atenderPedidoRecurso(char pId, char* data, int sockfd);
void atenderPedidoInfoItem(char* data, int sockfd);
void notificacionBloqueo(int socketPersonaje);
void notificacionDarRecurso(int personajeSockfd);
void actualizar_posicion(char pId, indicaciones_t* ind);
int personajeEnCaja(char pId, char rId);
void setSockEscucha();
void atenderNotificacionNivelFinalizado(char pId, int sockfd, fd_set* masterfds);
void mandarLiberadosAOrquestador(t_list* recursosLiberados);
t_list* esperarAsignadosPorOrquestador();
void actualizarEstado(t_list* asignados);
void gestionarDeadlock();
void notificarDeadlock();
void separador_log(char* head);
void inicializarInterfazGrafica();
void recibirHandshake(int sockfd);
void dibujarPersonajes();

#endif /* NIVEL_H_ */
