/*
 * Personaje.h
 *
 *  Created on: 24/04/2013
 *      Author: utnso
 */

#ifndef PERSONAJE_H_
#define PERSONAJE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <commons/config.h>
#include <commons/log.h>
#include <commons/sockets.h>
#include <commons/collections/list.h>

#define CONFIG_PATH "./Config.txt"
#define LOG_PATH "./log.txt"
#define EJE_X "ejeX"
#define EJE_Y "ejeY"
#define POS "+"
#define NEG "-"
#define ORDEN_MUERTE 0
#define RECURSO_CONSEGUIDO 1

typedef struct {
	t_list *recursos;
	char *nombre;
} nivel_t;

typedef struct personaje_conf_t {
	char *nombre;
	char *simbolo;
	int vidas;
	char *orquestadoraddr;
	char *orquestadorport;
	char *detalleLog;
	t_list *planNiveles;
} PERSONAJE_CONF;

typedef void (*funcPtr)();

t_config *config;
t_log *logFile;
PERSONAJE_CONF *configObj;
coordenadas_t coordPersonaje, *coordRecurso;
int orquestadorSockfd, planificadorSockfd, nivelSockfd, punteroNivelActual = 0,
		punteroRecursoActual = 0, reiniciarNivel = 1;
char **parametrosMain;

PERSONAJE_CONF *getPersonajeConfig(void);
void configObj_destroy(void);
void nivel_destroy(nivel_t *self);
int connectOrquestador(void);
int connectNivel(ip_info_t *ipInfo);
int connectPlanificador(ip_info_t *ipInfo);
void requestConexiones(char *nombreNivel, ip_info_t **planificador,
		ip_info_t **nivel);
char *getObjetivosKey(char *key);
void personaje_ipInfo_deserializer(char *serialized, ip_info_t **ipNivel,
		ip_info_t **ipPlan);
coordenadas_t *requestCoordenadas(char *nombreRecurso);
void moversePorEje(char *eje, signed int unidades);
void moverseAlRecurso(coordenadas_t *coordRecurso);
int pedirRecurso(char *nombreRecurso);
void rutinaMuerte(int n);
void conectarProceso(char *nivel);
void solicitarMovimiento(indicaciones_t *ind);
char *getSimbolo(int number);
indicaciones_t *getIndicacionesObj(char *eje, char *sentido);
void rutinaVidas(void);
void mostrarMensaje(int n);
void notificarReinicioPlan(void);
void notificarReinicioNivel(void);
void notificarMuerte(void);
void notificarBloqueo(char *recurso);
void actualizarUbicacion(indicaciones_t *ind);
int alcanzoObjetivo(coordenadas_t *coordObjetivo);
void moverse(coordenadas_t *coordRecurso);
void notificarFinTurno(void);
int enviarSimbolo(int sockfd);
void hacerHandshake(int sockfd);
int esperarDesbloqueo(void);
int contabilizarObjetivos(char *objetivosKey);
void notificarNivelFinalizado(void);
void finalizarNivel(char *nombreNivel);
int personajeConectado(void);
void notificarFinPlanNiveles(void);

#endif /* PERSONAJE_H_ */
