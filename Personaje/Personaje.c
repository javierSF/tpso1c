/*
 * Personaje.c
 *
 *  Created on: 24/04/2013
 *      Author: utnso
 */

#include "Personaje.h"

int main(int argc, char **argv) {
	signal(SIGUSR1, (funcPtr) rutinaVidas);
	signal(SIGTERM, (funcPtr) rutinaMuerte);
	parametrosMain = argv;

	configObj = getPersonajeConfig();
	t_log_level detalle = log_level_from_string(configObj->detalleLog);
	logFile = log_create(LOG_PATH, "ProcesoPersonaje", false, detalle);

	while (punteroNivelActual < configObj->planNiveles->elements_count) { //loop para cada nivel en la planificación.
		int recursoOtorgado = 0, personajeDesbloqueado = 1;
		nivel_t *nivelActual = list_get(configObj->planNiveles,
				punteroNivelActual);

		conectarProceso(nivelActual->nombre);
		coordPersonaje.ejeX = 0;
		coordPersonaje.ejeY = 0;

		while (punteroRecursoActual < nivelActual->recursos->elements_count) { //loop para cada objetivo del nivel seleccionado.
			char *recursoActual = list_get(nivelActual->recursos,
					punteroRecursoActual);
			coordRecurso = requestCoordenadas(recursoActual);
			moverseAlRecurso(coordRecurso);
			recursoOtorgado = pedirRecurso(recursoActual);

			if (recursoOtorgado) {
				punteroRecursoActual++;
				if (punteroRecursoActual
						< nivelActual->recursos->elements_count)
					notificarFinTurno();
				log_info(logFile, "Recurso %s conseguido.", recursoActual);

			} else {
				notificarBloqueo(recursoActual);
				personajeDesbloqueado = esperarDesbloqueo();
				if (personajeDesbloqueado) {
					recursoOtorgado = 1;
					punteroRecursoActual++;
				}
			}

			if (!personajeDesbloqueado)
				break; //se le ordenó morir.

			coordenadas_destroy(coordRecurso);
		}

		if (personajeConectado()) {
			switch (recursoOtorgado) {
			case RECURSO_CONSEGUIDO:
				finalizarNivel(nivelActual->nombre);
				break;
			case ORDEN_MUERTE:
				rutinaMuerte(0);
				if (reiniciarNivel) {
					punteroRecursoActual = 0;
				} else {
					punteroNivelActual = 0;
					punteroRecursoActual = 0;
				}
				break;
			}
		} else if (reiniciarNivel) {
			punteroRecursoActual = 0;
		} else {
			punteroNivelActual = 0;
			punteroRecursoActual = 0;
		}
	}

	notificarFinPlanNiveles();

	configObj_destroy();
	config_destroy(config);
	log_destroy(logFile);

	return EXIT_SUCCESS;
}

void notificarFinPlanNiveles(void) {
	header_t header;

	orquestadorSockfd = connectOrquestador();
	hacerHandshake(orquestadorSockfd);
	header.type = NOTIFICACION_FIN_PLAN_NIVELES;
	header.length = 0;
	sockets_send(orquestadorSockfd, &header, '\0');
	close(orquestadorSockfd);
}

int personajeConectado(void) {
	return planificadorSockfd != -1 && nivelSockfd != -1;
}

void finalizarNivel(char *nombreNivel) {
	notificarNivelFinalizado();
	close(planificadorSockfd);
	close(nivelSockfd);
	log_info(logFile, "%s finalizado.", nombreNivel);
	punteroNivelActual++;
	punteroRecursoActual = 0;
}

void notificarNivelFinalizado(void) {
	header_t header;
	header.type = NOTIFICACION_NIVEL_FINALIZADO;
	header.length = 0;
	sockets_send(nivelSockfd, &header, '\0');
	//TODO:
	sockets_send(planificadorSockfd, &header, '\0');
}

int enviarSimbolo(int sockfd) {
	header_t header;
	header.type = NOTIFICACION_DATOS_PERSONAJE;
	header.length = strlen(configObj->simbolo) + 1;

	return sockets_send(sockfd, &header, configObj->simbolo);
}

void moverseAlRecurso(coordenadas_t *coordObjetivo) {
	header_t header;

	do {
		recv(planificadorSockfd, &header, sizeof(header), MSG_WAITALL);

		switch (header.type) {
		case NOTIFICACION_MOVIDA:
			moverse(coordObjetivo);
			break;
		default:
			log_warning(logFile, "Mensaje inesperado.");
		}

		if (!personajeConectado())
			break;

		if (!alcanzoObjetivo(coordObjetivo))
			notificarFinTurno();
	} while (!alcanzoObjetivo(coordObjetivo));
}

/**
 * Espera por el desbloqueo del proceso.
 * Si devuelve 1 el proceso  fue desbloqueado y le fue otorgado el recurso que esperaba.
 * Si devuelve 0 se le ha ordenado morir.
 */
int esperarDesbloqueo(void) {
	header_t header;
	char *data;
	int result = 0;
	int nbytes = recv(planificadorSockfd, &header, sizeof(header), MSG_WAITALL);

	if (nbytes == 0) {
		printf("Planificador desconectado.");
//		exit(EXIT_FAILURE);
	}
	switch (header.type) {
	case NOTIFICACION_MUERTE:
		result = 0;
		break;
	case RESPUESTA_PEDIDO_RECURSO:
		data = malloc(header.length);
		nbytes = recv(planificadorSockfd, data, header.length, MSG_WAITALL);
		if (strcmp(data, "yes") == 0) {
			result = 1;
		}

		free(data);
		break;
	}

	return result;
}

int alcanzoObjetivo(coordenadas_t *coordObjetivo) {
	return coordObjetivo->ejeX == coordPersonaje.ejeX
			&& coordObjetivo->ejeY == coordPersonaje.ejeY;
}

void moverse(coordenadas_t *coordObjetivo) {
	int unidadesX, unidadesY;

	unidadesX = coordObjetivo->ejeX - coordPersonaje.ejeX;
	unidadesY = coordObjetivo->ejeY - coordPersonaje.ejeY;

	if (unidadesX != 0) {
		moversePorEje(EJE_X, unidadesX);
	}

	if (unidadesY != 0 && unidadesX == 0) {
		moversePorEje(EJE_Y, unidadesY);
	}
}

void notificarFinTurno(void) {
	header_t header;
	header.type = NOTIFICACION_MOVIMIENTO_REALIZADO;
	header.length = 0;
	sockets_send(planificadorSockfd, &header, '\0');
}

indicaciones_t *getIndicacionesObj(char *eje, char *sentido) {
	indicaciones_t *ind = malloc(sizeof(indicaciones_t));

	ind->eje = malloc(strlen(eje) + 1);
	ind->sentido = malloc(strlen(sentido) + 1);
	strcpy(ind->eje, eje);
	strcpy(ind->sentido, sentido);

	return ind;
}

char *getSimbolo(int number) {
	if (number < 0) {
		return NEG;
	} else {
		return POS;
	}
}

void moversePorEje(char *eje, signed int unidades) {
	indicaciones_t *ind = getIndicacionesObj(eje, getSimbolo(unidades));
	solicitarMovimiento(ind);
	actualizarUbicacion(ind);
	indicaciones_destroy(ind);
}

void actualizarUbicacion(indicaciones_t *ind) {
	if (strcmp(ind->eje, EJE_X) == 0 && strcmp(ind->sentido, POS) == 0)
		coordPersonaje.ejeX++;
	if (strcmp(ind->eje, EJE_X) == 0 && strcmp(ind->sentido, NEG) == 0)
		coordPersonaje.ejeX--;
	if (strcmp(ind->eje, EJE_Y) == 0 && strcmp(ind->sentido, POS) == 0)
		coordPersonaje.ejeY++;
	if (strcmp(ind->eje, EJE_Y) == 0 && strcmp(ind->sentido, NEG) == 0)
		coordPersonaje.ejeY--;

	log_info(logFile, "Posicion actual (%d, %d)", coordPersonaje.ejeX,
			coordPersonaje.ejeY);
}

void solicitarMovimiento(indicaciones_t *ind) {
	header_t header;
	header.type = MOVIMIENTO;
	char *data = indicaciones_serializer(ind, &header.length);
	sockets_send(nivelSockfd, &header, data);
}

/**
 * Devuelve 1 si le fue otorgado el recurso.
 * Devuelve 0 si el recurso no fue otorgado.
 */
int pedirRecurso(char *nombreRecurso) {
	header_t header;
	int result = 0;
	header.type = PEDIDO_RECURSO;
	header.length = strlen(nombreRecurso) + 1;

	sockets_send(nivelSockfd, &header, nombreRecurso);
	recv(nivelSockfd, &header, sizeof(header), MSG_WAITALL);

	if (header.type == RESPUESTA_PEDIDO_RECURSO) {
		char *respuesta = malloc(header.length);

		recv(nivelSockfd, respuesta, header.length, MSG_WAITALL);

		if (strcmp(respuesta, "yes") == 0) {
			result = 1;
		}

		free(respuesta);
	}

	log_info(logFile, "Pedido recurso %s", nombreRecurso);

	if (result) {
		log_info(logFile, "Respuesta a pedido recurso: otorgado");
	} else {
		log_info(logFile, "Respuesta a pedido recurso: no otorgado");
	}

	return result;
}

coordenadas_t *requestCoordenadas(char *nombreRecurso) {
	header_t header;
	coordenadas_t *coordRecurso = NULL;

	header.type = PEDIDO_INFO_ITEM;
	header.length = strlen(nombreRecurso) + 1;
	sockets_send(nivelSockfd, &header, nombreRecurso);
	recv(nivelSockfd, &header, sizeof(header), MSG_WAITALL);

	if (header.type == RESPUESTA_INFO_ITEM) {
		char *coordSerialized = malloc(header.length);

		recv(nivelSockfd, coordSerialized, header.length, MSG_WAITALL);
		coordRecurso = coordenadas_deserializer(coordSerialized);
		free(coordSerialized);
	}

	return coordRecurso;
}

char *getObjetivosKey(char *key) {
	char *objetivosKey = malloc(strlen("obj[") + strlen(key) + strlen("]") + 1);

	strcpy(objetivosKey, "obj[");
	strcat(objetivosKey, key);
	strcat(objetivosKey, "]");

	return objetivosKey;
}

void requestConexiones(char *nombreNivel, ip_info_t **planificador,
		ip_info_t **nivel) {
	header_t h;
	int nbytes;
	h.type = PEDIDO_INFO_CONEXION;
	h.length = strlen(nombreNivel) + 1;

	sockets_send(orquestadorSockfd, &h, nombreNivel);
	nbytes = recv(orquestadorSockfd, &h, sizeof(h), MSG_WAITALL);

	if (h.type == RESPUESTA_PEDIDO_INFO_CONEXION) {
		char *data = malloc(h.length);
		recv(orquestadorSockfd, data, h.length, MSG_WAITALL);
		personaje_ipInfo_deserializer(data, nivel, planificador);
		free(data);
	}

	if (nbytes == 0) {
		log_error(logFile, "Datos de %c no recibidos.", nombreNivel);
	}
}

void conectarProceso(char *nivel) {
	ip_info_t *conInfoPlan, *conInfoNivel;
	orquestadorSockfd = connectOrquestador();
	hacerHandshake(orquestadorSockfd);
	requestConexiones(nivel, &conInfoPlan, &conInfoNivel);
	close(orquestadorSockfd);
	log_info(logFile, "Desconectado del orquestador.");
	planificadorSockfd = connectPlanificador(conInfoPlan);
	hacerHandshake(planificadorSockfd);
	enviarSimbolo(planificadorSockfd);
	ipInfo_destroy(conInfoPlan);
	nivelSockfd = connectNivel(conInfoNivel);
	hacerHandshake(nivelSockfd);
	enviarSimbolo(nivelSockfd);
	ipInfo_destroy(conInfoNivel);
}

void hacerHandshake(int sockfd) {
	header_t header;
	header.type = HANDSHAKE_PERSONAJE;
	header.length = 0;
	sockets_send(sockfd, &header, '\0');
	recv(sockfd, &header, sizeof(header), MSG_WAITALL);

	switch (header.type) {
	case HANDSHAKE_PLANIFICADOR:
		log_info(logFile, "Conectado al Planificador.");
		break;
	case HANDSHAKE_ORQUESTADOR:
		log_info(logFile, "Conectado al Orquestador.");
		break;
	case HANDSHAKE_NIVEL:
		log_info(logFile, "Conectado al Nivel.");
		break;
	default:
		log_error(logFile, "Error en el handshake.");
	}
}

void personaje_ipInfo_deserializer(char *serialized, ip_info_t **ipNivel,
		ip_info_t **ipPlan) {
	ip_info_t *ip1 = malloc(sizeof(ip_info_t));
	int offset = 0, tmp_size = 0;

	for (tmp_size = 1; (serialized)[tmp_size - 1] != '\0'; tmp_size++)
		;
	ip1->addr = malloc(tmp_size);
	memcpy(ip1->addr, serialized, tmp_size);
	offset = tmp_size;

	for (tmp_size = 1; (serialized + offset)[tmp_size - 1] != '\0'; tmp_size++)
		;
	ip1->port = malloc(tmp_size);
	memcpy(ip1->port, serialized + offset, tmp_size);
	offset += tmp_size;

	ip_info_t *ip2 = malloc(sizeof(ip_info_t));
	for (tmp_size = 1; (serialized + offset)[tmp_size - 1] != '\0'; tmp_size++)
		;
	ip2->addr = malloc(tmp_size);
	memcpy(ip2->addr, serialized + offset, tmp_size);
	offset += tmp_size;

	for (tmp_size = 1; (serialized + offset)[tmp_size - 1] != '\0'; tmp_size++)
		;
	ip2->port = malloc(tmp_size);
	memcpy(ip2->port, serialized + offset, tmp_size);
	offset += tmp_size;

	*ipNivel = ip1;
	*ipPlan = ip2;
}

int connectOrquestador() {
	int orquestadorSockfd = sockets_createClient(configObj->orquestadoraddr,
			configObj->orquestadorport);

	if (orquestadorSockfd == -1) {
		log_error(logFile, "Conexion con orquestador. IP: %s puerto: %s",
				configObj->orquestadoraddr, configObj->orquestadorport);
//		exit(EXIT_FAILURE);
	} else {
		log_info(logFile, "Conectado con orquestador. IP: %s puerto: %s",
				configObj->orquestadoraddr, configObj->orquestadorport);
	}

	return orquestadorSockfd;
}

int connectNivel(ip_info_t *ipInfo) {
	int sockfd = sockets_createClient(ipInfo->addr, ipInfo->port);

	if (sockfd == -1) {
		log_error(logFile, "Conexion con Nivel. IP: %s puerto: %s",
				ipInfo->addr, ipInfo->port);
		exit(EXIT_FAILURE);
	} else {
		log_info(logFile, "Conectado con Nivel. IP: %s puerto: %s",
				ipInfo->addr, ipInfo->port);
	}

	return sockfd;
}

int connectPlanificador(ip_info_t *ipInfo) {
	int sockfd = sockets_createClient(ipInfo->addr, ipInfo->port);

	if (sockfd == -1) {
		log_error(logFile, "Conexion con planificador. IP: %s puerto: %s",
				ipInfo->addr, ipInfo->port);
		exit(EXIT_FAILURE);
	} else {
		log_info(logFile, "Conectado con planificador. IP: %s puerto: %s",
				ipInfo->addr, ipInfo->port);
	}

	return sockfd;
}

PERSONAJE_CONF *getPersonajeConfig(void) {
	config = config_create(CONFIG_PATH);
	PERSONAJE_CONF *configObj = malloc(sizeof(PERSONAJE_CONF));

	configObj->nombre = malloc(
			strlen(config_get_string_value(config, "nombre")) + 1);
	configObj->simbolo = malloc(
			strlen(config_get_string_value(config, "simbolo")) + 1);
	strcpy(configObj->nombre, config_get_string_value(config, "nombre"));
	strcpy(configObj->simbolo, config_get_string_value(config, "simbolo"));
	configObj->vidas = config_get_int_value(config, "vidas");
	char *tok = ":";
	char *orqaddr = strdup(config_get_string_value(config, "orquestador"));
	configObj->orquestadoraddr = strtok(orqaddr, tok);
	configObj->orquestadorport = strtok(NULL, tok);
	configObj->detalleLog = config_get_string_value(config, "detalleLog");
	configObj->planNiveles = list_create();
	int i, j;

	for (i = 0; config_get_array_value(config, "planDeNiveles")[i] != NULL ;
			i++) {
		char *nivel = config_get_array_value(config, "planDeNiveles")[i];
		nivel_t *nuevoNivel = malloc(sizeof(nivel_t));
		nuevoNivel->nombre = malloc(strlen(nivel) + 1);
		strcpy(nuevoNivel->nombre, nivel);
		nuevoNivel->recursos = list_create();
		char *key = getObjetivosKey(nivel);

		for (j = 0; config_get_array_value(config, key)[j] != NULL ; j++) {
			char *recurso = config_get_array_value(config, key)[j];
			char *nuevoRecurso = malloc(strlen(recurso) + 1);
			strcpy(nuevoRecurso, recurso);
			list_add_in_index(nuevoNivel->recursos, j, (void *) nuevoRecurso);
		}

		free(key);
		list_add_in_index(configObj->planNiveles, i, nuevoNivel);
	}

	return configObj;
}

void nivel_destroy(nivel_t *self) {
	list_destroy_and_destroy_elements(self->recursos, (void *) free);
	free(self->nombre);
	free(self);
}

void configObj_destroy(void) {
	free(configObj->orquestadoraddr);
	free(configObj->nombre);
	free(configObj->simbolo);
	list_destroy_and_destroy_elements(configObj->planNiveles,
			(void *) nivel_destroy);
	free(configObj);
}

void rutinaMuerte(int n) {
	notificarMuerte();
	mostrarMensaje(n);

	if (configObj->vidas == 0) {
		configObj->vidas = config_get_int_value(config, "vidas");
		notificarReinicioPlan();
		reiniciarNivel = 0;
		//reiniciar plan de niveles
	} else {
		configObj->vidas--;
		notificarReinicioNivel();
		reiniciarNivel = 1;
		//reiniciar el nivel actual
	}

	printf("Cantidad de vidas: %d\n", configObj->vidas);

	close(nivelSockfd);
	close(planificadorSockfd);
	nivelSockfd = -1;
	planificadorSockfd = -1;
}

void rutinaVidas(void) {
	configObj->vidas += 1;
	printf("Vida otorgada\n");
	printf("Cantidad de vidas: %d\n", configObj->vidas);
}

void mostrarMensaje(int n) {
	if (n == SIGTERM) {
		printf("Muerte por señal SIGTERM\n");
	} else {
		printf("Muerte por Bloqueo\n");
	}
}

void notificarReinicioPlan(void) {
	header_t header;
	header.type = NOTIFICACION_REINICIAR_PLAN;
	header.length = 0;
	sockets_send(planificadorSockfd, &header, '\0');
}

void notificarReinicioNivel(void) {
	header_t header;
	header.type = NOTIFICACION_REINICIAR_NIVEL;
	header.length = 0;
	sockets_send(planificadorSockfd, &header, '\0');
}

void notificarMuerte(void) {
	header_t header;
	header.type = NOTIFICACION_MUERTE; //notificar muerte.
	header.length = 0;
	sockets_send(nivelSockfd, &header, '\0');
}

void notificarBloqueo(char *recurso) {
	header_t header;
	header.type = NOTIFICACION_BLOQUEO;
	header.length = strlen(recurso) + 1;
	int bytes = sockets_send(planificadorSockfd, &header, recurso);

	if (bytes == 0) {
		log_error(logFile, "envio notificacion bloqueo al planificador.");
		close(planificadorSockfd);
		planificadorSockfd = -1;
	}
}
